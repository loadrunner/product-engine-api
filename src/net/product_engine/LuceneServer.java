package net.product_engine;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.facet.taxonomy.TaxonomyWriter;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyReader;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyWriter;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.util.Version;

import net.product_engine.common.BaseServer;

public class LuceneServer extends BaseServer {
	private Directory _index = null;
	private Directory _taxo_index = null;
	private IndexSearcher _searcher = null;
	private TaxonomyReader _taxo_reader = null;
	private boolean _searcher_is_current = false;
	private boolean _taxo_is_current = false;
	private static LuceneServer _instance = null;
	
	public static LuceneServer singleton() {
		if (_instance == null && init_run) {
			_instance = new LuceneServer();
			if(_instance._init()) {
				_instance.start();
			} else {
				_instance = null;
			}
			
		}
		
		return _instance;
	}
	
	protected boolean _init() {
		if (!init_run) {
			return false;
		}
		
		try {
			//TODO: get path from config
			_index = new MMapDirectory(new File("data/index1"));
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		if (!DirectoryReader.indexExists(_index)) {
			try {
				getIndexWriter(true).close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		
		try {
			//TODO: get path from config
			_taxo_index = new MMapDirectory(new File("data/taxo1"));
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public Directory getIndex() {
		return _index;
	}
	public Directory getTaxoIndex() {
		return _taxo_index;
	}
	
	public IndexWriter getIndexWriter() throws IOException {
		return getIndexWriter(false);
	}
	public IndexWriter getIndexWriter(boolean clearIndex) throws IOException {
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, getAnalizer());
		config.setWriteLockTimeout(10000);
		
		if (clearIndex) {
			// Create a new index in the directory, removing any
			// previously indexed documents:
			config.setOpenMode(OpenMode.CREATE);
		} else {
			// Add new documents to an existing index:
			config.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}
		IndexWriter w = new IndexWriter(getIndex(), config);
		
		return w;
	}
	
	public TaxonomyWriter getTaxoWriter() throws IOException {
		return getTaxoWriter(false);
	}
	public TaxonomyWriter getTaxoWriter(boolean clearIndex) throws IOException {
		TaxonomyWriter taxo;
		
		if (clearIndex) {
			taxo = new DirectoryTaxonomyWriter(getTaxoIndex(), OpenMode.CREATE);
		} else {
			taxo = new DirectoryTaxonomyWriter(getTaxoIndex(), OpenMode.CREATE_OR_APPEND);
		}
		
		return taxo;
	}
	
	public IndexSearcher getSearcher() throws IOException {
		if ((_searcher == null || !_searcher_is_current) && _index != null ) {
			if(_searcher != null) {
				_searcher.getIndexReader().close();
				_searcher = null;
				_taxo_is_current = false;
			}
			IndexReader reader = DirectoryReader.open(_index);
			System.out.println("Index has " + reader.numDocs() + " documents.");//485830
			_searcher = new IndexSearcher(reader);
			_searcher_is_current = true;
		}
		
		return _searcher;
	}
	public TaxonomyReader getTaxoReader() throws IOException {
		if ((_taxo_reader == null || !_taxo_is_current) && _taxo_index != null ) {
			if(_taxo_reader != null) {
				_taxo_reader.close();
				_taxo_reader = null;
			}
			_taxo_reader = new DirectoryTaxonomyReader(_taxo_index);
			_taxo_is_current = true;
		}
		
		return _taxo_reader;
	}
	
	public void markSearcherAsOld() {
		_searcher_is_current = false;
	}
	
	//private StandardAnalyzer analyzer = null;
	public Analyzer getAnalizer() {
	//	if (analyzer == null) {
			StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
	//	}
		
		return analyzer;
	}

}
