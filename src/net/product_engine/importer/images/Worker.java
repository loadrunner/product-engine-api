package net.product_engine.importer.images;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

import net.product_engine.common.Pair;
import net.product_engine.importer.BaseWorker;
import net.product_engine.models.Merchant;
import net.product_engine.models.Product;
import net.product_engine.sql.MysqlConnection;

public class Worker extends BaseWorker {
	private int _rows_count = 0;
	private AtomicInteger _save_count = new AtomicInteger(0);
	private AtomicInteger _products_done_count = new AtomicInteger(0);
	private AtomicInteger _error_count = new AtomicInteger(0);
	private Vector<Pair<String, String>> _error_list = new Vector<Pair<String, String>>();
	private ArrayList<Product> _products = new ArrayList<Product>();
	public static String destination_folder;
	
	public Worker(Merchant merchant) {
		super(merchant);
	}
	public Worker(Merchant merchant, int id) {
		super(merchant, id);
	}
	
	public void run() {
		_log.info("thread " + _id + ": starting worker thread; merchant " + _merchant.name + " (" + _merchant.id +  ")");
		
		try {
			_getProducts();
		} catch (SQLException e) {
			_log.warning("thread " + _id + ": " + e.toString());
			_result = "Error: " + e.toString();
		//	return;_products.size()
		//	daca nu sunt produse, oricum nu face nimic pentu ca iese la while
		}
		
		final String merchant_folder = destination_folder + "/" + ((int)(_merchant.id/1000)) + "/" +  _merchant.id;
		if (!(new File(merchant_folder)).exists()) {
			_log.info("thread " + _id + ": merchant folder not found");
			if (!(new File(merchant_folder)).mkdirs()) {
				_log.warning("thread " + _id + ": cannot create merchant folder: " + merchant_folder);
				_result = "Error. Cannot create merchant folder (" + merchant_folder + ")";
				return;
			}
		}
		
		_steps = 0;
		_total_steps = _products.size();
		ArrayList<Thread> downloaders = new ArrayList<Thread>();
		while (_steps < _total_steps) {
			if (_steps < _total_steps) {//TODO: fix logic
				/*
				final Product p = _products.get(_steps);
				Thread thread = new Thread() {
					public void run() {
						Pair<Integer, String> result = p.processImages(merchant_folder);
						if (result.first > 0) {
							_save_count.addAndGet(result.first);
							_error_count.set(0);
							_error_list.clear();
						} else {
							_error_count.incrementAndGet();
							_error_list.add(new Pair<String, String>(p.getImageUrlsString(), result.second));
						}
						_products_done_count.incrementAndGet();
					}
				};
				thread.start();
				downloaders.add(thread);
				*/
				_steps++;
			} else {
			//	_log.fine("thread " + _id + ": no image download slots. waiting (active downloads " + MerchantProduct.active_downloads.get() + ")");
				try {
					Thread.sleep(2000);
				} catch (Exception e) { }
			}
			
			Iterator<Thread> itr = downloaders.iterator();
			while(itr.hasNext()) {
				Thread thread = itr.next();
				if (!thread.isAlive()) {
					_log.fine("thread " + _id + ": image thread finished; deleting");
					itr.remove();
				}
			}
			
			if (_error_count.get() >= 20) {
				_send_message();
				break;
			}
		}
		
		//waiting for active threads to finish
		while (downloaders.size() > 0) {
			Iterator<Thread> itr = downloaders.iterator();
			while(itr.hasNext()) {
				Thread thread = itr.next();
				if (!thread.isAlive()) {
					_log.fine("thread " + _id + ": image thread finished; deleting");
					itr.remove();
				}
			}
			try {
				_log.fine("thread " + _id + ": waiting for active image threads to finish");
				Thread.sleep(2000);
			} catch (Exception e) { }
		}
		
		try {
			_merchant.updateImagesProcessingDate();
		} catch (SQLException e) {
			_log.warning("thread " + _id + ": " + e.toString());
		}
		
		String errors = "";
		
		for(int i = 0; i < _error_list.size(); i++) {
			errors += _error_list.get(i).first + " - " + _error_list.get(i).second + ", ";
		}
		
		_result = _rows_count + " products found, " +
				_products_done_count.get() + " products processed, " +
				_save_count.get() + " images saved" +
				(errors.length() > 0 ? ", errors: " + errors : "");
		
		_log.info("thread " + _id + ": merchant " + _merchant.name + " (" + _merchant.id +  "): worker done. statistics: " + _result);
	}
	
	private void _getProducts() throws SQLException {
		_log.fine("thread " + _id + ": starting to select products with images waiting to be imported.");
		
		Connection db_link = MysqlConnection.getConnection("live");
		int rows = 0;
		String q = " SELECT id, image_url FROM product pm " +
				"WHERE merchant_id = ? AND status = 1 AND imported_image IS NULL " +
				"AND (LENGTH(IFNULL(image_url, '')) > 10 AND image_url LIKE 'http%') " +
				"ORDER BY id ASC ";
		
		PreparedStatement s = db_link.prepareStatement(q);
		s.setInt(1, _merchant.id);
		ResultSet r = s.executeQuery();
		
		while (r.next()) {
			//_products.add(new Product(r));
			rows++;
		}
		_rows_count = rows;
		
		_log.fine("thread " + _id + ": " + rows + " products found.");
	}
	private void _send_message() {
		//TODO: send email
	}
}
