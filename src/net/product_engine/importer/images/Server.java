package net.product_engine.importer.images;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.product_engine.common.Util;
import net.product_engine.importer.BaseServer;
import net.product_engine.importer.images.Worker;
import net.product_engine.models.Merchant;
import net.product_engine.sql.MysqlConnection;


public class Server extends BaseServer {
	private static int _merchants_max_active = 50;
	private static int _max_workers = 1;
	private static Server _instance = null;
	
	public static Server singleton() {
		if (_instance == null && init_run) {
			_instance = new Server();
			if(_instance._init()) {
				_instance.start();
			} else {
				_instance = null;
			}
			
		}
		
		return _instance;
	}
	
	public void run() {
		int nr_threads = 0;
		Merchant merchant;
		while (_active.get()) {
			if (merchants.size() > 0) {
				if (workers.size() < _max_workers) {
					_log.fine("starting new thread (" + workers.size() + " active)");
					merchant = merchants.poll();
					try {
						Worker thread = new Worker(merchant, ++nr_threads);
						thread.start();
						workers.add(thread);
					} catch (Exception e) {
						_log.warning(e.toString());
					}
				} else {
					_log.finer("no slots available (" + workers.size() + " threads active, " + merchants.size() + " merchants remaining) zzz...");
				}
			} else {
				_log.finer("no merchants ready (" + workers.size() + " threads active, " + merchants.size() + " merchants remaining) zzz...");
			}
			
			//condition to skip the delay when there are no merchants in queue
			if (merchants.size() != 0 || workers.size() > 0) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					_log.warning(e.toString());
					break;
				}
			} else {
				pause();
			}
			
			cleanupWorkerQueue();
		}
		
		_log.info("Done importing. main thread dying (threads still " + workers.size() + " active)");
	}
	@Override
	public void recharge() {
		recharge(_merchants_max_active);
	}
	public void recharge(int nr) {
		if (nr <= 0) {
			recharge();
		}
		if (workers.size() < nr) {
			try {
				String id_merchants = Util.toString(getQueuedMerchants()); 
				
				nr = nr - workers.size();
				_log.fine("adding " + nr + " merchant to queue");
				String q = "SELECT * FROM merchant m WHERE ";
				Connection db_link = MysqlConnection.getConnection("live");
				ResultSet r;
				PreparedStatement s;
				
				s = db_link.prepareStatement(
					q + "status = 1 " +
					"AND (product_images_downloaded_at IS NULL OR TIMESTAMPDIFF(HOUR, product_images_downloaded_at, NOW()) > ?) " +
					"AND product_feed_url IS NOT NULL " +
					(id_merchants.length() > 0 ? " AND id NOT IN (" + id_merchants + ") " : " ") +
					"ORDER BY product_images_downloaded_at ASC " +
					"LIMIT 0, " + nr
					);
				s.setInt(1, 48);
				
				int rows = 0;
				r = s.executeQuery();
				while (r.next()) {
					rows++;
					int id = r.getInt("id");
					
					if (checkMerchantExistsInQueue(id)) {
						continue;
					}
					
					String host = r.getString("name");
					
					_log.fine("added merchant " + host + "(" + id + ") to queue");
					Merchant m = new Merchant(id, host);
					merchants.add(m);
				}
				
				if (rows != 0) {
					force();
				} else {
					_log.fine("no merchants found");
				}
			} catch (Exception e) {
				_log.severe("cannot get merchants! (" + e.toString() + ")");
			}
		} else {
			_log.fine("merchant queue full");
		}
	}
	
	protected boolean _init() {
		if (!super._init()) {
			return false;
		}
		
		Worker.destination_folder = getConfig("importer.images.destination_folder");
		
		try {
			_max_workers = Integer.parseInt(getConfig("importer.images.max_threads"));
		} catch (Exception e) {
			_log.warning("importer.images.max_threads: Invalid value!");
		}
		/*
		try {
			//Product.max_active_downloads = Integer.parseInt(getConfig("importer.images.max_threads_per_merchant"));
		} catch (Exception e) {
			_log.warning("importer.images.max_threads_per_merchant: Invalid value!");
		}
		*/
		return true;
	}
	protected void createScheduler() {
		if (getConfig("importer.environment") == null
		 || !getConfig("importer.environment").equals("LIVE")) {
			_log.warning("not live");
			return;
		}
		/*
		// Creates a Scheduler instance.
		Scheduler s = new Scheduler();
		// Schedule a once-a-minute task.
		s.schedule("30 00,02,04,22  * * *", new Runnable() {
			public void run() {
				singleton().recharge();
			}
		});
		s.setDaemon(true); 
		// Starts the scheduler.
		s.start();
		*/
	}
}
