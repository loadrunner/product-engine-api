package net.product_engine.importer.products;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;

import net.product_engine.LuceneServer;
import net.product_engine.common.Currency;
import net.product_engine.common.Util;
import net.product_engine.importer.BaseServer;
import net.product_engine.importer.products.Worker;
import net.product_engine.models.Merchant;
import net.product_engine.sql.MysqlConnection;


import it.sauronsoftware.cron4j.Scheduler;

public class Server extends BaseServer {
	public static int vat_value = 24;
	public static String default_currency = null;
	private static int _merchants_last_update_diff = 12; //hours
	private static int _merchants_max_active = 50;
	private static int _max_workers = 1;
	private static Server _instance = null;
	
	public static Server singleton() {
		if (_instance == null && init_run) {
			_instance = new Server();
			if(_instance._init()) {
				_instance.start();
			} else {
				_instance = null;
			}
			
		}
		
		return _instance;
	}
	
	protected boolean _init() {
		if (!super._init()) {
			return false;
		}
		
		try {
			_merchants_last_update_diff = Integer.parseInt(getConfig("importer.products.merchants.last_update_diff"));
		} catch (Exception e) {
			_log.warning("importer.merchants.last_update_diff: Invalid value!");
		}
		try {
			_merchants_max_active = Integer.parseInt(getConfig("importer.products.merchants.max_active"));
		} catch (Exception e) {
			_log.warning("importer.merchants.max_active: Invalid value!");
		}
		try {
			MerchantFeedDownloader.max_active_downloads = Integer.parseInt(getConfig("importer.products.max_active_downloads"));
		} catch (Exception e) {
			_log.warning("importer.max_active_downloads: Invalid value!");
		}
		try {
			_max_workers = Integer.parseInt(getConfig("importer.products.max_threads"));
		} catch (Exception e) {
			_log.warning("importer.max_threads: Invalid value!");
		}
		try {
			vat_value = Integer.parseInt(getConfig("importer.products.vat_value"));
		} catch (Exception e) {
			_log.warning("importer.products.vat_value: Invalid value!");
		}
		try {
			default_currency = getConfig("importer.products.default_currency");
		} catch (Exception e) {
			_log.warning("importer.products.default_currency: Invalid value!");
		}
		
		Currency.init();
		
		MerchantFeedDownloader.singleton();
		
		return true;
	}
	
	@Override
	public void run() {
	//	int x = 0;
		int nr_threads = 0;
		Merchant merchant;
		LuceneServer lucene = LuceneServer.singleton();
		
		while (_active.get()) {
			if (!LuceneServer.singleton().isAlive()) {
				_log.finer("lucene not ready. zzz...");
			}
			else if (merchants.size() > 0) {
				if (workers.size() < _max_workers) {
					_log.fine("starting new thread (" + workers.size() + " active)");
					merchant = merchants.poll();
					MerchantFeedDownloader.left_to_process.decrementAndGet();
					try {
						Worker thread = new Worker(merchant, lucene.getIndexWriter(), lucene.getTaxoWriter(), ++nr_threads);
						thread.start();
						workers.add(thread);
					} catch (Exception e) {
						_log.warning(e.toString());
					}
				} else {
					_log.finer("no slots available (" + workers.size() + " threads active, " + MerchantFeedDownloader.left_to_process.get() + " merchants remaining) zzz...");
				}
			} else {
				_log.finer("no merchants ready (" + workers.size() + " threads active, " + MerchantFeedDownloader.left_to_process.get() + " merchants remaining) zzz...");
			}
			
			//condition to skip the delay when there are no merchants in queue
			if (MerchantFeedDownloader.left_to_process.get() != 0 || merchants.size() != 0 || workers.size() > 0) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					_log.warning(e.toString());
					break;
				}
			} else {
				LuceneServer.singleton().markSearcherAsOld();
				pause();
			}
			
			cleanupWorkerQueue();
		}
		
		_log.info("Done importing. main thread dying (threads still " + workers.size() + " active)");
	}
	public void recharge() {
		if (_merchants_max_active > 0) {
			recharge(_merchants_max_active);
		}
	}
	public void recharge(int nr) {
		if (nr == 0) {
			recharge();
			return;
		}
		if (MerchantFeedDownloader.left_to_process.get() < nr) {
			try {
				String id_merchants = Util.toString(getQueuedMerchants()); 
				
				nr = nr - MerchantFeedDownloader.left_to_process.get();
				_log.fine("adding " + nr + " merchants to queue");
				String q = "SELECT m.* FROM merchant m WHERE ";
				Connection db_link = MysqlConnection.getConnection("live");
				ResultSet r;
				PreparedStatement s;
				
				s = db_link.prepareStatement(
					q + "m.status = 1 " +
					"AND (m.product_feed_processed_at IS NULL OR TIMESTAMPDIFF(HOUR, m.product_feed_processed_at, NOW()) > ? * (m.product_feed_errors + 1)) " +
					"AND m.product_feed_url IS NOT NULL " +
					(id_merchants.length() > 0 ? " AND m.id NOT IN (" + id_merchants + ") " : " ") +
					"ORDER BY m.product_feed_processed_at ASC " +
					"LIMIT 0, " + nr
					);
				s.setInt(1, _merchants_last_update_diff);
				
				int rows = 0;
				r = s.executeQuery();
				while (r.next()) {
					rows++;
					int id = r.getInt("id");
					
					if (checkMerchantExistsInQueue(id)) {
						continue;
					}
					
					String host = r.getString("name");
					String url = r.getString("product_feed_url");
					String file_type = r.getString("product_feed_type");
					String sep = r.getString("product_feed_csv_separator");
					char field_separator = sep != null && sep.length() > 0 ? sep.charAt(0) : ',';
					String field_corelation = r.getString("product_feed_field_corelation");
					boolean vat_included = r.getString("product_feed_vat_included").equals("1") ? true : false;
					String currency = r.getString("currency");
					
					_log.fine("added merchant " + host + "(" + id + "," + file_type + ") to queue");
					Merchant m = new Merchant(id, host, url, file_type, field_separator, field_corelation, vat_included, currency);
					(MerchantFeedDownloader.singleton()).add(m);
				}
				
				if (rows == 0) {
					_log.fine("no merchants found");
				}
			} catch (Exception e) {
				_log.severe("cannot get merchants! (" + e.toString() + ")");
			}
		} else {
			_log.fine("merchant queue full");
		}
	}
	@Override
	public boolean checkMerchantExistsInQueue(int id) {
		return getQueuedMerchants(false).contains(id);
	}
	@Override
	public HashSet<Integer> getQueuedMerchants() {
		return getQueuedMerchants(true);
	}
	public HashSet<Integer> getQueuedMerchants(boolean include_download_queue) {
		HashSet<Integer> ids = super.getQueuedMerchants();
		
		if (include_download_queue && (MerchantFeedDownloader.singleton()).getQueueSize() > 0) {
			ids.addAll((MerchantFeedDownloader.singleton()).getQueuedMerchantIds());
		}
		return ids;
	}
	
	protected void createScheduler() {
		if (getConfig("importer.environment") == null
		 || !getConfig("importer.environment").equals("LIVE")) {
			_log.warning("not live");
			return;
		}
		
		// Creates a Scheduler instance.
		Scheduler s = new Scheduler();
		// Schedule a once-a-minute task.
		s.schedule("00 00,01,02,03  * * *", new Runnable() {
			public void run() {
				singleton().recharge();
			}
		});
		s.setDaemon(true); 
		// Starts the scheduler.
		s.start();
	}
}
