package net.product_engine.importer.products.filetypes;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import net.product_engine.importer.products.filetypes.items.CustomXmlItem;
import net.product_engine.importer.products.filetypes.items.fields.base.BaseField;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class XmlFile extends BaseFile {
	protected Stack<HashMap<String, String>> _products = new Stack<HashMap<String, String>>();
	protected int _current_pos;
	protected int _max_pos = 0;
	protected String _xml_product_tag;
	protected String _xml_id_tag;
	protected HashSet<String> _merchant_codes = new HashSet<String>();
	
	public XmlFile(int merchant_id, String url, String field_corelation) throws Exception {
		super(merchant_id, url, field_corelation);
		
		_xml_product_tag = "produs";
		_xml_id_tag = "code";
	}
	
	public CustomXmlItem fetch() {
		if (_max_pos <= 0 || _current_pos >= _max_pos) {
			return null;
		}
		CustomXmlItem item = new CustomXmlItem(_products.pop(), _field_corelation);
		BaseField field = item.get(_xml_id_tag);
		
		if (field != null) {
			if (_merchant_codes.contains(field.get())) {
				item.setError("duplicate");
			} else {
				_merchant_codes.add(field.get());
			}
		}
		_current_pos++;
		
		return item;
	}
	
	private class CustomHandler extends DefaultHandler {
		private HashMap<String, String> _row;
		private boolean _inside = false;
		private StringBuilder _tempVal;
		
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			_tempVal = new StringBuilder();
			if (qName.equalsIgnoreCase(_xml_product_tag)) {
				_inside = true;
				_row = new HashMap<String, String>();
				return;
			}
		}
		
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (qName.equalsIgnoreCase(_xml_product_tag)) {
				_inside = false;
				_products.push(_row);
				return;
			}
			
			if (_inside) {
				_row.put(qName, _tempVal.toString());
			}
		}
		
		public void characters(char[] ch, int start, int length) throws SAXException {
		//	_tempVal = new String(ch,start,length);
			if (_tempVal != null) {
				for (int i=start; i< start + length; i++) {
					_tempVal.append(ch[i]);
				}
			}
		}
	}
	
	@Override
	protected void _init() throws Exception {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		saxParser.parse(new File(this._filename), new CustomHandler());
		
		_current_pos = 0;
	//	_max_pos = Math.min(_products.getLength(), 25000);
		_max_pos = _products.size();
	}
	@Override
	protected String _getExtension() {
		return "xml";
	}
	@Override
	protected Map<String, String> _detect_field_corelation() throws Exception {
		if (_products.size() == 0) {
			throw new Exception("No products! Cannot run field detection!");
		}
		Map<String, String> header = _products.pop();
		_max_pos--;
		
		Map<String, String> map = new HashMap<String, String>();
		
		for (String key : header.keySet()) {
			map.put(key, key);
		}
		
		return detect_field_corelation(map);
	}
}
