package net.product_engine.importer.products.filetypes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import org.apache.commons.io.IOUtils;

public class InvalidXmlFile extends XmlFile {
	public InvalidXmlFile(int merchant_id, String url, String field_corelation) throws Exception {
		super(merchant_id, url, field_corelation);
	}
	
	@Override
	protected boolean _downloadFile() throws IOException  {
		super._downloadFile();
		
		File file = new File(_filename);
		/*
		RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
		
		long fpointer = randomAccessFile.getFilePointer();
		String lineData = "";
		
		while ((lineData = randomAccessFile.readLine()) != null) {
			fpointer = randomAccessFile.getFilePointer() - lineData.length() - 2;
			if (lineData.indexOf(match) > 0) {
				_log.fine("Changing string in file " + file.getName());
				randomAccessFile.seek(fpointer);
				randomAccessFile.writeBytes(replacingString);
				// if the replacingString has less number of characters than the matching string line then enter blank spaces.
				if (replacingString.length() < int difference = (lineData.length() - replacingString.length())+1;
					for(int i=0; i < ");
			}
		}*/
		/*
		byte[] buffer = new byte[(int) file.length()];
		BufferedInputStream fr = new BufferedInputStream(new FileInputStream(file));
		fr.read(buffer);
		fr.close();
		String str = new String(buffer, "UTF-8");
		buffer = null;
		*/
		FileInputStream fis = new FileInputStream(file);
		StringWriter sow = new StringWriter();
		IOUtils.copy(fis, sow, "UTF-8");
		String str = sow.toString();
		fis = null;
		sow = null;
		
		str = str.replaceAll("&", "&amp;");
		//allowed: [\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD]
		//old: "([\\x00-\\x08]|\\x0B|\\x0C|[\\x0E-\\x19])"
		//stackoverflow: "(?<![\\uD800-\\uDBFF])[\\uDC00-\\uDFFF]|[\\uD800-\\uDBFF](?![\\uDC00-\\uDFFF])|[\\x00-\\x08\\x0B\\x0C\\x0E-\\x1F\\x7F-\\x9F\\uFEFF\\uFFFE\\uFFFF]";
		//old php: '![^\x09|\x0A|\x0D|[\x20-\x{D7FF}]|[\x{E000}-\x{FFFD}]|[\x{10000}-\x{10FFFF}]]+!u'
		//actual: ([\\x00-\\x08]|\\x0B|\\x0C|[\\x0E-\\x19]|\\x1d|\\x1e|\\x1f|[\\uD800-\\uDFFF])
		str = str.replaceAll("(?<![\\uD800-\\uDBFF])[\\uDC00-\\uDFFF]|[\\uD800-\\uDBFF](?![\\uDC00-\\uDFFF])|[\\x00-\\x08\\x0B\\x0C\\x0E-\\x1F\\x7F-\\x9F\\uFEFF\\uFFFE\\uFFFF]", " ");
	//	str = str.replaceAll("![\\x09|\\x0A|\\x0D|[\\x20-\\xD7FF]|[\\xE000-\\xFFFD]|[\\x10000-\\xFFFF]]", " ");
		FileWriter fw = new FileWriter(file);
		fw.write(str);
		fw.close();
		
		_log.fine("merchant " + _merchant_id +  ": file process complete");
		return true;
	}
}
