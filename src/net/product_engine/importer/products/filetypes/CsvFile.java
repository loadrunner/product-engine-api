package net.product_engine.importer.products.filetypes;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import net.product_engine.importer.products.filetypes.items.CustomCsvItem;
import net.product_engine.importer.products.filetypes.items.fields.base.BaseField;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class CsvFile extends BaseFile {
	protected CSVParser _products;
	protected Iterator<CSVRecord> _iterator;
	protected int _max_pos = 0;
	protected char _separator;
	protected char _encapsulator;
	protected HashSet<String> _merchant_codes = new HashSet<String>();
	
	public CsvFile(int merchant_id, String url, String field_corelation, char separator) throws IOException {
		this(merchant_id, url, field_corelation, separator, '"');
	}
	public CsvFile(int merchant_id, String url, String field_corelation, char separator, char encapsulator) throws IOException {
		super(merchant_id, url, field_corelation);
		_separator = separator;
		_encapsulator = encapsulator;
	}
	
	@Override
	public CustomCsvItem fetch() {
		try {
			if (!_iterator.hasNext()) {
				return null;
			}
			
			CSVRecord product = _iterator.next();
			if (product != null) {
				CustomCsvItem item = new CustomCsvItem(product, _field_corelation);
				BaseField field = item.get("code");
				
				if (field != null) {
					if (_merchant_codes.contains(field.get())) {
						item.setError("duplicate");
					} else {
						_merchant_codes.add(field.get());
					}
				}
				return item;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	protected void _init() throws Exception {
		_products = new CSVParser(new FileReader(this._filename),
				CSVFormat.newFormat(_separator)
					.withQuote(_encapsulator)
					.withIgnoreEmptyLines(true)
					.withAllowMissingColumnNames(true)
					.withRecordSeparator("\r\n"));
		_iterator = _products.iterator();
		//_max_pos = 50;
	}
	@Override
	protected String _getExtension() {
		return "csv";
	}
	@Override
	protected Map<String, String> _detect_field_corelation() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		
		if (_iterator.hasNext()) {
			CSVRecord header = _iterator.next();
			
			for (int i = 0; i < header.size(); i++) {
				map.put(Integer.toString(i), header.get(i));
			}
		}
		
		return detect_field_corelation(map);
	}
}
