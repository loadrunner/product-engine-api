package net.product_engine.importer.products.filetypes;

import java.io.IOException;

import org.apache.commons.csv.CSVRecord;

import net.product_engine.importer.products.filetypes.items.CustomCsvItem;
import net.product_engine.importer.products.filetypes.items.DouaParaleCsvItem;
import net.product_engine.importer.products.filetypes.items.fields.base.BaseField;


public class DouaParaleCsvFile extends CsvFile {
	public DouaParaleCsvFile(int merchant_id, String url, String field_corelation, char separator) throws IOException {
		super(merchant_id, url, field_corelation, separator);
	}
	
	@Override
	public CustomCsvItem fetch() {
		try {
			if (!_iterator.hasNext()) {
				return null;
			}
			
			CSVRecord product = _iterator.next();
			if (product != null) {
				DouaParaleCsvItem item = new DouaParaleCsvItem(product, _field_corelation);
				BaseField field = item.get("code");
				
				if (field != null) {
					if (_merchant_codes.contains(field.get())) {
						item.setError("duplicate");
					} else {
						_merchant_codes.add(field.get());
					}
				}
				return item;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	protected String _getExtension() {
		return "2parale.csv";
	}
}
