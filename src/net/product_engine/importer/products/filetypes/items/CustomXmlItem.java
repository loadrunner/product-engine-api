package net.product_engine.importer.products.filetypes.items;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class CustomXmlItem extends BaseItem {
	private HashMap<String, String> _data;
	
	public CustomXmlItem(HashMap<String, String> e, Map<String, String> field_corelation) {
		super();
		_data = e;
		
		Set<String> keys = _field_list.keySet();
		Iterator<String> iter = keys.iterator();
		String key;
		
		while (iter.hasNext()) {
			key = (String)iter.next();
			if (field_corelation.containsKey(key)) {
				_field_list.get(key).set(_get(field_corelation.get(key)));
			}// else {
			//	if (_field_list.get(key).isStrict()) {
				//	_log.finer("field " + key + " missing! from xml!");
			//	} else {
				//	_log.finest("field " + key + " missing! from xml!");
			//	}
			//}
		}
	}
	
	protected String _get(String key) {
		if (_data.containsKey(key)) {
			return _data.get(key);
		}
		
		return "";
	}
}
