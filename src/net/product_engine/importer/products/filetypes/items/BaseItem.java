package net.product_engine.importer.products.filetypes.items;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import net.product_engine.common.Currency;
import net.product_engine.importer.products.Server;
import net.product_engine.importer.products.filetypes.items.fields.*;
import net.product_engine.importer.products.filetypes.items.fields.base.*;

public abstract class BaseItem {
	protected final Map<String, BaseField> _field_list =
		new HashMap<String, BaseField>() {
		private static final long serialVersionUID = 1L; {
			put("code", new StringField(100, true));
			put("name", new StringField(200, true));
			put("brand", new BrandField());
			put("category", new StringField(200, true, true));
			put("url", new UrlField(true));
			put("price", new PriceField());
			put("image_url", new UrlField());
			put("description", new StringField(5000));
			put("stock", new IntField(4, 4, 1));
		}
	};
	protected HashSet<String> _errors = new HashSet<String>();
	
	public BaseItem() {
		_init();
	}
	
	protected void _init() {
		
	}
	
	public String[] getFields() {
		String[] fields = new String[_field_list.size()];
		Set<String> keys = _field_list.keySet();
		Iterator<String> iter = keys.iterator();
		int i = 0;
		while (iter.hasNext()) {
			fields[i] = (String)iter.next();
			i++;
		}
		
		return fields;
	}
	public void addVat() {
		try {
			BaseField price = _field_list.get("price");
			price.set(Double.toString(Double.parseDouble(price.get()) * (100+Server.vat_value) / 100));
		} catch (NumberFormatException e) { }
	}
	public void cleanupName() {
		String name = _field_list.get("name").get();
		if (name != null && name.length() > 2) {
			String old_name = name;
			String brand = _field_list.get("brand").get();
			if (brand != null && brand.length() > 2) {
				name = name.replaceAll("\\ ?(?i)" + Pattern.quote(brand) + "\\ ?", " ");
			}
			String category = _field_list.get("category").get();
			if (category != null && category.length() > 3) {
				name = name.replaceAll("\\ ?(?i)" + Pattern.quote(category) + "\\ ?", " ");
			}
			if (!name.equals(old_name)) {
				_field_list.get("name").set(name.trim());
			}
		}
	}
	public void setCurrency(String currency) {
		BaseField price = _field_list.get("price");
		Double c = Currency.getRate(currency);
		if (c != 1) {
			price.set(Double.toString(Double.parseDouble(price.get()) * c));
		}
	}
	
	public BaseField get(String key) {
		if (_field_list.containsKey(key)) {
			BaseField field = _field_list.get(key);
			if (!field.validate()) {
				setError(key);
			}
			return field;
		}
		return null;
	}
	public String getErrors() {
		return _errors.toString();
	}
	public boolean hasErrors() {
		return !_errors.isEmpty();
	}
	public boolean hasError(String key) {
		return _errors.contains(key);
	}
	public boolean setError(String value) {
		if (!_errors.contains(value)) {
			_errors.add(value);
			return true;
		}
		return false;
	}
}
