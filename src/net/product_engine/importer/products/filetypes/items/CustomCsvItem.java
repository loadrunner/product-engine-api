package net.product_engine.importer.products.filetypes.items;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVRecord;

public class CustomCsvItem extends BaseItem {
	private CSVRecord _data;
	
	public CustomCsvItem(CSVRecord l, Map<String, String> field_corelation) {
		super();
		_data = l;
		
		Set<String> keys = _field_list.keySet();
		Iterator<String> iter = keys.iterator();
		String key;
		
		while (iter.hasNext()) {
			key = (String)iter.next();
			
			if (field_corelation.containsKey(key)) {
				_field_list.get(key).set(_get(Integer.parseInt(field_corelation.get(key))));
			}// else {
			//	if (_field_list.get(key).isStrict()) {
				//	_log.finer("field " + key + " missing! from csv!");
			//	} else {
				//	_log.finest("field " + key + " missing! from csv!");
			//	}
			//}
		}
	}
	private String _get(int key) {
		if (key >= 0 && _data.size() > key) {
			return _data.get(key);
		}
		return "";
	}
}
