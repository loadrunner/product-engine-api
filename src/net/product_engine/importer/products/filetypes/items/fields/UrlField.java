package net.product_engine.importer.products.filetypes.items.fields;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;

import net.product_engine.importer.products.filetypes.items.fields.base.StringField;

public class UrlField extends StringField {
	public UrlField() {
		super(500, false);
	}
	public UrlField(boolean strict) {
		super(500, strict);
	}
	
	@Override
	public void _set(String value) {
		super._set(value.trim().replace(" ", "%20"));
	}
	@Override
	protected boolean _validate() {
		if (_raw_value != null && _raw_value.length() > 15
		&& _raw_value.matches("^(http|https)://(.*)$")) {
			return true;
		}
		return false;
	}
	public Field getDocumentField(String key, boolean stored) {
		return new StoredField(key, _value);
	}
}
