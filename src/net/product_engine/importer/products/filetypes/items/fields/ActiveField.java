package net.product_engine.importer.products.filetypes.items.fields;

import net.product_engine.importer.products.filetypes.items.fields.base.IntField;

public class ActiveField extends IntField {
	
	public ActiveField() {
		super();
		_value = 1;
	}
	public ActiveField(Integer max) {
		super(max);
		_value = 1;
	}
	public ActiveField(Integer max, boolean strict) {
		super(0, max, 0, strict);
		_value = 1;
	}
}
