package net.product_engine.importer.products.filetypes.items.fields.base;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;

public abstract class BaseField {
	protected String _raw_value = null;
	protected boolean _strict = false;
	protected boolean _facet = false;
	
	public boolean validate() {
		if (_strict && !_validate()) {
			return false;
		}
		return true;
	}
	public void set(String value) {
		_raw_value = value;
		
		if (_validate()) {
			_set(_raw_value);
		}
	}
	public boolean isStrict() {
		return _strict;
	}
	public boolean hasFacet() {
		return _facet;
	}
	public abstract String get();
	public abstract Field getDocumentField(String key, boolean stored);
	public abstract FieldType getType();
	protected abstract void _set(String value);
	protected abstract boolean _validate();
}
