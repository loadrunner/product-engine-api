package net.product_engine.importer.products.filetypes.items.fields;

import net.product_engine.importer.products.filetypes.items.fields.base.IntField;

public class StockField extends IntField {
	
	public StockField() {
		super();
		_value = 1;
	}
	
	@Override
	public void set(String value) {
		if (value.equals("true")) {
			super.set("1");
		} else if (value.equals("false")) {
			super.set("2");
		} else {
			super.set(value);
		}
	}
}
