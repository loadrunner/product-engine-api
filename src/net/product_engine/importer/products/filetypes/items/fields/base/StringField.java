package net.product_engine.importer.products.filetypes.items.fields.base;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;

public class StringField extends BaseField {
	protected String _value = null;
	protected int _length = 0;
	
	public StringField() {
	}
	public StringField(String def) {
		_value = def;
	}
	public StringField(int length) {
		_length = length;
	}
	public StringField(String def, int length) {
		_value = def;
		_length = length;
	}
	public StringField(int length, boolean strict) {
		_length = length;
		_strict = strict;
	}
	public StringField(int length, boolean strict, boolean facet) {
		_length = length;
		_strict = strict;
		_facet = facet;
	}
	
	@Override
	public String get() {
		return _value;
	}
	@Override
	public void _set(String value) {
		value = StringEscapeUtils.unescapeHtml(StringEscapeUtils.unescapeHtml(value));
		value = value.replaceAll("\\<.*?\\>", "");
	//	value = StringEscapeUtils.escapeHtml(value);
		
		if (_length > 0 && value.length() > _length) {
			value = value.substring(0, _length - 1);
		}
		_value = value;
	}
	
	@Override
	protected boolean _validate() {
		if (_raw_value != null && _raw_value.length() > 0) {
			return true;
		}
		return false;
	}
	@Override
	public FieldType getType() {
		return TextField.TYPE_STORED;
	}
	@Override
	public Field getDocumentField(String key, boolean stored) {
		return new TextField(key, _value, stored ? Field.Store.YES : Field.Store.NO);
	}
}
