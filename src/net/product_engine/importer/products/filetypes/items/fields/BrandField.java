package net.product_engine.importer.products.filetypes.items.fields;

import net.product_engine.importer.products.filetypes.items.fields.base.StringField;

public class BrandField extends StringField {
	public BrandField() {
		super(100, false, true);
	}
	@Override
	public void _set(String value) {
		super._set(value);
		
		if (_value.equals("-")) {
			_value = null;
		}
	}
}
