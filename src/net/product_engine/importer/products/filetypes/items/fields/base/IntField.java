package net.product_engine.importer.products.filetypes.items.fields.base;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;

public class IntField extends BaseField {
	protected int _value;
	protected Integer _max = null;
	protected Integer _min = null;
	
	public IntField() {
		this(0);
	}
	public IntField(int def) {
		this(def, null);
	}
	public IntField(int def, Integer max) {
		this(def, max, null);
	}
	public IntField(int def, Integer max, Integer min) {
		this(def, max, min, false);
	}
	public IntField(int def, Integer max, Integer min, boolean strict) {
		_value = def;
		_max = max;
		_min = min;
		_strict = strict;
	}
	
	@Override
	public String get() {
		return Integer.toString(_value);
	}
	@Override
	public void _set(String value) {
		if (_max != null && Integer.valueOf(value) > _max) {
			_value = _max;
		} else if (_min != null && Integer.valueOf(value) < _min) {
			_value = _min;
		} else {
			_value = Integer.valueOf(value);
		}
	}
	@Override
	protected boolean _validate() {
		try {
			Integer.valueOf(_raw_value);
		} catch (NumberFormatException e) {
		//	if (isStrict()) {
			//	_log.finer("field validation failed; value :" + _raw_value);
		//	} else {
			//	_log.finest("field validation failed; value :" + _raw_value);
		//	}
			return false;
		}
		return true;
	}
	@Override
	public FieldType getType() {
		return org.apache.lucene.document.IntField.TYPE_STORED;
	}
	@Override
	public Field getDocumentField(String key, boolean stored) {
		return new org.apache.lucene.document.IntField(key, _value, stored ? Field.Store.YES : Field.Store.NO);
	}
}
