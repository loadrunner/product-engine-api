package net.product_engine.importer.products.filetypes.items.fields.custom;

import net.product_engine.importer.products.filetypes.items.fields.UrlField;

public class DouaParaleUrlField extends UrlField {
	
	@Override
	public void set(String value) {
		super.set(value.split(",")[0]);
	}
}
