package net.product_engine.importer.products.filetypes.items.fields.custom;

import java.util.regex.Pattern;

import net.product_engine.importer.products.filetypes.items.fields.PriceField;


public class DouaParalePriceField extends PriceField {
	
	@Override
	public void set(String value) {
		super.set(_parse(value));
	}
	
	protected static String _parse(String value) {
		if(value.equals("price")) {
			return value;
		}
		Pattern pattern = Pattern.compile("([0-9]+)(\\.|,)?([0-9]+)?");
		java.util.regex.Matcher m = pattern.matcher(value);
		
		if(!m.find()) {
			return "";
		}
		
		String price="";
		try {
			price = m.group(1);
			if(m.group(3) != null) {
				price += "." + m.group(3);
			}
		} catch (Exception e) {
			
		}
		
		return price;
	}
}
