package net.product_engine.importer.products.filetypes.items.fields;

import net.product_engine.importer.products.filetypes.items.fields.base.StringField;

public class ConditionField extends StringField {
	@Override
	protected boolean _validate() {
		return !(_raw_value == null
			|| !_raw_value.matches("^(nou|desigilat|resigilat|aproape\\ nou|folosit|reconditionat|reparat|defect)$"));
	}
}
