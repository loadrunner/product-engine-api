package net.product_engine.importer.products.filetypes.items.fields;

import net.product_engine.importer.products.filetypes.items.fields.base.DecimalField;

public class PriceField extends DecimalField {
//	private boolean _add_vat = false;
	
	public PriceField() {
		super(12, 2, true, true);
	}
	@Override
	public String get() {
		_nf.setMaximumFractionDigits(2);
		return _nf.format(_value);
	}
	@Override
	protected boolean _validate() {
		if (!super._validate()) {
			return false;
		}
		return Double.valueOf(_raw_value) > 0;
	}
}
