package net.product_engine.importer.products.filetypes.items.fields.base;

import java.text.NumberFormat;

import org.apache.lucene.document.DoubleField;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;

public class DecimalField extends BaseField {
	protected NumberFormat _nf = NumberFormat.getNumberInstance();
	protected double _value = 0;
	
	public DecimalField() {
		this(0D);
	}
	public DecimalField(Double def) {
		this(def, 10);
	}
	public DecimalField(int digits) {
		this(digits, 2);
	}
	public DecimalField(Double def, int digits) {
		this(def, digits, 2);
	}
	public DecimalField(int digits, int decimals) {
		this(0D, digits, decimals);
	}
	public DecimalField(Double def, int digits, int decimals) {
		this(def, digits, decimals, false);
	}
	public DecimalField(int digits, int decimals, boolean strict) {
		this(0D, digits, decimals, strict);
	}
	public DecimalField(int digits, int decimals, boolean strict, boolean facet) {
		this(0D, digits, decimals, strict, facet);
	}
	public DecimalField(Double def, int digits, int decimals, boolean strict) {
		this(def, digits, decimals, strict, false);
	}
	public DecimalField(Double def, int digits, int decimals, boolean strict, boolean facet) {
		_nf.setGroupingUsed(false);
		_nf.setMaximumIntegerDigits(digits);
		_nf.setMaximumFractionDigits(decimals);
		_value = def;
		_strict = strict;
		_facet = facet;
	}
	
	@Override
	public String get() {
		return _nf.format(_value);
	}
	@Override
	public void _set(String value) {
		_value = Double.valueOf(value);
	}
	@Override
	protected boolean _validate() {
		try {
			Double.valueOf(_raw_value);
		} catch (Exception e) {
			if (_raw_value != null && _raw_value.contains(",")) {
				_raw_value = _raw_value.replace(",", "");
			//	_log.finest("field validation failed; removing \",\", result: " + _raw_value);
				return _validate();
			}
		//	if (isStrict()) {
			//	_log.finer("field validation failed; value :" + _raw_value);
		//	} else {
			//	_log.finest("field validation failed; value :" + _raw_value);
		//	}
			return false;
		}
		return true;
	}
	@Override
	public FieldType getType() {
		return DoubleField.TYPE_STORED;
	}
	@Override
	public Field getDocumentField(String key, boolean stored) {
		return new DoubleField(key, _value, stored ? Field.Store.YES : Field.Store.NO);
	}
}
