package net.product_engine.importer.products.filetypes.items;

import java.util.Map;

import org.apache.commons.csv.CSVRecord;

import net.product_engine.importer.products.filetypes.items.fields.StockField;
import net.product_engine.importer.products.filetypes.items.fields.custom.DouaParalePriceField;
import net.product_engine.importer.products.filetypes.items.fields.custom.DouaParaleUrlField;


public class DouaParaleCsvItem extends CustomCsvItem {
	public DouaParaleCsvItem(CSVRecord l, Map<String, String> field_corelation) {
		super(l, field_corelation);
	}
	
	protected void _init() {
		_field_list.put("price", new DouaParalePriceField());
		_field_list.put("image_url", new DouaParaleUrlField());
		
		_field_list.put("stock", new StockField());
	}
}
