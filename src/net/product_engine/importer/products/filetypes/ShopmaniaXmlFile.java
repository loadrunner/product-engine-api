package net.product_engine.importer.products.filetypes;

public class ShopmaniaXmlFile extends XmlFile {
	public ShopmaniaXmlFile(int merchant_id, String url, String field_corelation) throws Exception {
		super(merchant_id, url, field_corelation);
		
		_xml_product_tag = "shopitem";
		_xml_id_tag = "part_number";
	}
}
