package net.product_engine.importer.products.filetypes;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Logger;

import net.product_engine.common.BaseServer;
import net.product_engine.common.Util;
import net.product_engine.importer.products.filetypes.items.BaseItem;
import net.product_engine.models.Merchant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public abstract class BaseFile {
	protected static final Logger _log = Logger.getLogger(BaseServer.namespace);
	protected Map<String, String> _field_corelation = null;
	protected int _merchant_id = -1;
	protected String _url = null;
	protected String _filename = null;
	
	public BaseFile(int merchant_id, String url, Map<String, String> field_corelation) throws IOException {
		_merchant_id = merchant_id;
		_url = url;
		_field_corelation = field_corelation;
		
		_downloadFile();
	}
	public BaseFile(int merchant_id, String url) throws IOException {
		this(merchant_id, url, (Map<String, String>)null);
	}
	public BaseFile(int merchant_id, String url, String tc) throws IOException {
		this(merchant_id, url, process_field_corelation(tc)); 
	}
	
	public void init() throws Exception {
		_init();
		
		if (_field_corelation == null) {
			_field_corelation = _detect_field_corelation();
			try {
				(new Merchant(_merchant_id, null)).updateFieldCorelation((new JSONObject(_field_corelation).toString()));
			} catch (Exception e) {
				
			}
		}
	}
	public int getMerchantId() {
		return _merchant_id;
	}
	
	protected boolean _downloadFile() throws IOException  {
		_log.fine("merchant " + _merchant_id +  ": starting file download");
		
		_filename = BaseServer.getConfig("importer.temp_path") + "/import_file_" + _merchant_id + "_" + new java.util.Date().getTime() + "." + _getExtension();
		
	//	URLConnection uc = u.openConnection();
	//	String contentType = uc.getContentType();
	//	int contentLength = uc.getContentLength();
		
	//	Object o = u.getContent();
	//	Log.add("download", "I got a " + o.getClass().getName());
		
		int bufSize = 8 * 1024;
		
		URL u = new URL(_url);
		URLConnection uc = u.openConnection();
		uc.setConnectTimeout(30000);
		uc.setReadTimeout(3600000);
		uc.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.16) Gecko/20110929 Iceweasel/3.5.16 (like Firefox/3.5.16)");
		File destination = new File(_filename);
		
		BufferedInputStream fin = new BufferedInputStream(uc.getInputStream(), bufSize);
		BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(destination), bufSize);
		Util.copyPipe(fin, fout, bufSize);
		fin.close();
		fout.close();
		_log.fine("merchant " + _merchant_id +  ": file download complete");
		return true;
	}
	
	public abstract BaseItem fetch();
	protected abstract void _init() throws Exception;
	protected abstract String _getExtension();
	protected abstract Map<String, String> _detect_field_corelation() throws Exception;
	
	public static Map<String, String> process_field_corelation(String tc) {
		if (tc == null || tc.length() <= 4) {
			return null;
		}
		
		Map<String, String> field_corelation = new HashMap<String, String>();
		
		try {
			JSONObject tmp = new JSONObject(tc);
			JSONArray keys = tmp.names();
			
			for (int i = 0; i < keys.length(); i++) {
				try {
					field_corelation.put(keys.getString(i), tmp.getString(keys.getString(i)));
				} catch (JSONException e) {_log.warning(e.toString()); }
			}
		} catch (JSONException e) {
			_log.warning(e.toString());
			return null;
		}
		
		return field_corelation;
	}
	public static Map<String, String> detect_field_corelation(Map<String, String> header) {
		Map<String, String> tc = new HashMap<String, String>();
		HashSet<String> found = new HashSet<String>();
		
		for (Map.Entry<String, String> entry : header.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if (!found.contains("code") && value.matches("^(?i)(code|cod|id|codprodus|codprodus(mercant)?|part_number|product_id)$")) {
				tc.put("code", key);
				found.add("code");
			} else if (!found.contains("name") && value.matches("^(?i)(name|productname|nume|numeprodus|product|title)$")) {
				tc.put("name", key);
				found.add("name");
			} else if (!found.contains("brand") && value.matches("^(?i)(brand|marca|numemarca)$")) {
				tc.put("brand", key);
				found.add("brand");
			} else if (!found.contains("category") && value.matches("^(?i)(category|categorie|numecategorie)$")) {
				tc.put("category", key);
				found.add("category");
			} else if (!found.contains("description") && value.matches("^(?i)(description|desc|descriere)$")) {
				tc.put("description", key);
				found.add("description");
			} else if (!found.contains("price") && value.matches("^(?i)(price|pret|pretlei)$")) {
				tc.put("price", key);
				found.add("price");
			} else if (!found.contains("url") && value.matches("^(?i)(url|link|product_url|aff_code)$")) {
				tc.put("url", key);
				found.add("url");
			} else if (!found.contains("image_url") && value.matches("^(?i)(image|url_?image|image_?url|imagine|url_?imagine|poza|url_?poza|image_urls)$")) {
				tc.put("image_url", key);
				found.add("image_url");
			} else if (!found.contains("stock") && value.matches("^(?i)(stock|(in)?stoc|product_active)$")) {
				tc.put("stock", key);
				found.add("stock");
			} else {
				tc.put("unknown_" + key, key);
			}
		}
		_log.warning(tc.toString());
		return tc;
	}
}
