package net.product_engine.importer.products.filetypes;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Pattern;

import net.product_engine.importer.products.filetypes.items.CustomCsvItem;
import net.product_engine.importer.products.filetypes.items.fields.base.BaseField;


public class InvalidCsvFile extends BaseFile {
	private BufferedReader _products; 
	private int _current_pos;
	private int _max_pos = 0;
	private String _separator;
	private HashSet<String> _merchant_codes = new HashSet<String>();
	
	public InvalidCsvFile(int merchant_id, String url, String field_corelation, char separator) throws IOException {
		super(merchant_id, url, field_corelation);
		_separator = String.valueOf(separator);
	}
	
	@Override
	public CustomCsvItem fetch() {
		if (_max_pos != 0 && _current_pos >= _max_pos) {
			return null;
		}
		
		_current_pos++;
		
		try {
			String product = _products.readLine();
			
			if (product != null) {
				CustomCsvItem item = null;//new CustomCsvItem(product.split(Pattern.quote(_separator)), _field_corelation);
				BaseField field = item.get("code");
				
				if (field != null) {
					if (_merchant_codes.contains(field.get())) {
						item.setError("duplicate");
					} else {
						_merchant_codes.add(field.get());
					}
				}
				return item;
			} else {
				return null;
			}
		} catch (IOException e) {
			return null;
		}
	}
	
	@Override
	protected void _init() throws Exception {
		_products = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(this._filename))));
		_current_pos = 0;
		//_max_pos = 50;
	}
	@Override
	protected String _getExtension() {
		return "invalid.csv";
	}
	@Override
	protected Map<String, String> _detect_field_corelation() throws Exception {
		String line = _products.readLine();
		if (line != null) {
			String[] header = line.split(Pattern.quote(_separator));
			Map<String, String> map = new HashMap<String, String>();
			
			for (int i = 0; i < header.length; i++) {
				map.put(Integer.toString(i), header[i]);
			}
			
			return detect_field_corelation(map);
		}
		return null;
	}
}
