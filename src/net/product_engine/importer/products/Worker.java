package net.product_engine.importer.products;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.facet.index.CategoryDocumentBuilder;
import org.apache.lucene.facet.taxonomy.TaxonomyWriter;
import org.apache.lucene.facet.taxonomy.CategoryPath;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;

import net.product_engine.common.Util;
import net.product_engine.importer.BaseWorker;
import net.product_engine.importer.products.filetypes.BaseFile;
import net.product_engine.importer.products.filetypes.items.BaseItem;
import net.product_engine.importer.products.filetypes.items.fields.base.BaseField;
import net.product_engine.models.Merchant;

public class Worker extends BaseWorker {
	private int _rows_count = 0;
	private int _rows_error = 0;
//	private int _rows_new = 0;
	private int _rows_inserted = 0;
//	private int _rows_modified = 0;
	private int _rows_updated = 0;
	private int _rows_deleted = 0;
	private BaseFile _file = null;
//	private Connection _db_link = null;
	private IndexWriter _writer = null;
	private TaxonomyWriter _taxo = null;
	public long timestamp = 0;
	
	public Worker(Merchant merchant, IndexWriter writer, TaxonomyWriter taxo) {
		super(merchant);
		_writer = writer;
		_taxo = taxo;
	}
	public Worker(Merchant merchant, IndexWriter writer, TaxonomyWriter taxo, int i) {
		super(merchant, i);
		_writer = writer;
		_taxo = taxo;
	}
	
	public void run() {
		_total_steps = 6;
		timestamp = System.currentTimeMillis();
		_log.info("thread " + _id + ": starting worker thread; merchant " + _merchant.name + " (" + _merchant.id +  ")");
		
		try {
			_file = _merchant.getFile();
			_steps++;
		} catch (Exception e) {
			_log.warning("thread " + _id + ": " + e.toString());
			_result = "Error: " + e.toString();
			try {
				_merchant.updateProductProcessingInfo(_result, true);
			} catch (SQLException e1) {
				_log.warning("thread " + _id + ": updateProcessingInfo failed (" +e1.toString() + ")");
			}
			//_send_message(true);
			try {
				_writer.close();
			} catch (IOException e1) { }
			return;
		}
		
		try {
			_log.fine("thread " + _id + ": starting to delete old products");
			_writer.deleteDocuments(new Term("merchant_id", Integer.toString(_merchant.id)));
			_steps++;
			_log.fine("thread " + _id + ":  delete old products done");
		} catch (IOException e) {
			_log.warning("thread " + _id + ": delete old products failed (" + e.toString() + ")");
			_result = "Error: " + e.toString();
			try {
				_writer.rollback();
				_writer.close();
			} catch (IOException e2) { }
			try {
				_merchant.updateProductProcessingInfo(_result, true);
			} catch (SQLException e1) {
				_log.warning("thread " + _id + ": updateProcessingInfo failed (" +e1.toString() + ")");
			}
			//_send_message(true);
			return;
		}
		
		BaseItem product;
		CategoryDocumentBuilder cBuilder = new CategoryDocumentBuilder(_taxo);
		
		try {
			_log.fine("thread " + _id + ": starting product processing/insertion");
			while ((product = _file.fetch()) != null) {
				_rows_count++;
				_insertProduct(product, cBuilder);
			}
			_log.fine("thread " + _id + ": insert products done");
			
			_log.fine("thread " + _id + ": commiting taxo index");
			_taxo.commit();
			_steps++;
			_log.fine("thread " + _id + ": done commit taxo index");
			
			_log.fine("thread " + _id + ": commiting document index");
			_writer.commit();
			_steps++;
			_log.fine("thread " + _id + ": done commit document index");
			
			_taxo.close();
			_writer.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			_log.warning("thread " + _id + ": " + e.toString());
			_result = "Error: " + e.toString();
			try {
				_taxo.rollback();
				_taxo.close();
			} catch (IOException e2) { }
			try {
				_writer.rollback();
				_writer.close();
			} catch (IOException e2) { }
			try {
				_merchant.updateProductProcessingInfo(_result, true);
			} catch (SQLException e1) {
				_log.warning("thread " + _id + ": updateProcessingInfo failed (" +e1.toString() + ")");
			}
			//_send_message(true);
			return;
		}
		
		DecimalFormat twoDForm = new DecimalFormat("0.0");
		
		_result = 
			_rows_count + " products found in feed, " +
			_rows_error + " products with errors, " +
			_rows_inserted + " products inserted, " +
			_rows_updated + " products updated, " +
			_rows_deleted + " products deleted, " +
			"it took " + twoDForm.format((double) (System.currentTimeMillis() - timestamp) / 1000L) + " seconds";
		
		try {
			_merchant.updateProductProcessingInfo(_result, false);
			_steps++;
			_merchant.updateOfferDate();
			_steps++;
		} catch (SQLException e) {
			_log.warning("thread " + _id + ": " + e.toString());
		}
		
		_log.info("thread " + _id + ": merchant " + _merchant.name + " (" + _merchant.id +  "): worker done. statistics: " + _result);
		//_send_message(false);
	//	if (_rows_deleted > 10 && (_rows_count - _rows_error) < _rows_deleted) {
			//_send_warn_message("s-au dezactivat multe produse: " + _rows_deleted + ", total produse in feed: " + _rows_count + "");
	//	}
	}
	
	private boolean _insertProduct(BaseItem product, CategoryDocumentBuilder cBuilder) throws IOException {
		if (!_merchant.vat_included) {
			product.addVat();
		}
		if (!_merchant.currency.equals("RON")) {
			product.setCurrency(_merchant.currency);
		}
		
		product.cleanupName();
		
		String[] fields = product.getFields();
		
		Document doc = new Document();
		List<CategoryPath> categories = new ArrayList<CategoryPath>();
		BaseField field;
		String uid = Integer.toString(_merchant.id), key, value;
		
		doc.add(new org.apache.lucene.document.StringField("merchant_id", Integer.toString(_merchant.id), Field.Store.YES));
		categories.add(new CategoryPath("merchant_id", Integer.toString(_merchant.id)));
		doc.add(new org.apache.lucene.document.StringField("merchant", _merchant.name, Field.Store.YES));
		
		for (int i = 0; i < fields.length; i++) {
			key = fields[i];
			field = product.get(key);
			value = field.get();
			if (value != null) {
				doc.add(field.getDocumentField(key, true));
				
				if (field.hasFacet()) {
					categories.add(new CategoryPath(key, value));
				}
				
				if (key.equals("image_url")) {
					doc.add(new TextField("image_hash", Util.computeSum(value), Field.Store.NO));
				} else if (key.equals("code")) {
					uid += "." + value;
				}
			}
		//	_log.finest("key: " + fields[i] + ", value: \"" + field.get() + "\"");
		}
		
		doc.add(new org.apache.lucene.document.StringField("id", Base64.encodeBase64URLSafeString(uid.getBytes()), Field.Store.YES));
		
		if (product.hasErrors()) {
			_rows_error++;
			return false;
		}
		
		if (categories.size() > 0) {
			cBuilder.setCategoryPaths(categories).build(doc);
		}
		_writer.addDocument(doc);
		_rows_inserted++;
		
		return true;
	}
	/*
	private void _send_message(boolean fail) {
		try {
			Connection db_link = MysqlConnection.getConnection("live");//LIVE
		//	Connection db_link = MssqlConnection.getConnection("test");//TEST
			
			if (fail) {
				//pt user
				String subject = "Erori la importul produselor magazinului" + _merchant.name;
				String text = "Au apărut erori la actualizarea produselor magazinului " + _merchant.name + " din feedul " + _merchant.url_import + ".<br />" +
					"Lista este invalidă sau setările sunt greșite.<br /><br />" +
					"Găsești aici detalii complete despre pregătirea feedului:" +
					"<a href=\"http://www.cauti.ro/about/promote_feeddescription.html\">http://www.cauti.ro/about/promote_feeddescription.html</a><br /><br />" +
					"Corectează lista apoi fă setările necesare aici: <a href=\"http://www.cauti.ro/user/magazine/product_settings.html\">http://www.cauti.ro/user/magazine/product_settings.html</a>" +
					"<br /><br />Contactează-ne dacă ai nevoie de ajutor la pregatirea listei de produse.";
				String query = "INSERT INTO messages (message_template_id, type, target_id, sender_id, sender_name, subject, body, created_at)" + 
						"VALUES (?, 'MerchantMessage', ?, ?, 'Cauti.ro', ?, ?, NOW())";
				PreparedStatement insert = db_link.prepareStatement(query);
				insert.setString(1, BaseServer.getConfig("importer.message_templates.product_error_id"));
				insert.setInt(2, _merchant.id);
				insert.setString(3, BaseServer.getConfig("importer.support_user_id"));
				insert.setString(4, subject);
				insert.setString(5, text);
				insert.executeUpdate();
				/*
				//pt marketing
				Mail cauti_mail = new Mail();
				cauti_mail.addTo(BaseServer.getConfig("sh.save_ca.importer.marketing_email"));
				cauti_mail.setSubject("eroare import la merchantul " + _merchant.name);
				cauti_mail.setText("merchant: " + _merchant.name + ", id:  " + _merchant.id + "\n"
							+ "url: " + _merchant.url_import + "\n"
							+ "eroare import: " + _result;
				cauti_mail.send();
				*//*
			} else if (_manual_run) {
				String subject = "Rezultate import produse";
				String text = "[mesaj automat] - contacteaza-ne pentru nelamuriri\n\n" + _result;
				String query = "INSERT INTO messages (type, target_id, sender_id, sender_name, subject, body, created_at)" + 
						"VALUES ('MerchantMessage', ?, ?, 'Cauti.ro', ?, ?, NOW())";
				PreparedStatement insert = db_link.prepareStatement(query);
				insert.setInt(1, _merchant.id);
				insert.setString(2, BaseServer.getConfig("sh.save_ca.importer.support_user_id"));
				insert.setString(3, subject);
				insert.setString(4, text);
				insert.executeUpdate();
			}
		} catch (SQLException e) {
			_log.warning("message error: " + e.toString());
		}
	}
	private void _send_warn_message(String message) {
		try {
			Connection db_link = MysqlConnection.getConnection("live");//LIVE
		//	Connection db_link = MssqlConnection.getConnection("test");//TEST
			
			String subject = "avertizare problema la merchantul " + _merchant.name;
			String text = "merchant: " + _merchant.name + ", id:  " + _merchant.id + "<br />"
				+ "url: " + _merchant.url_import + "<br />"
				+ "mesaj: " + message;
			String query = "INSERT INTO messages (type, target_id, sender_id, sender_name, subject, body, created_at)" + 
					"VALUES ('UserMessage', ?, ?, 'Cauti.ro', ?, ?, NOW())";
			PreparedStatement insert = db_link.prepareStatement(query);
			insert.setString(1, BaseServer.getConfig("importer.support_user_id"));
			insert.setString(2, BaseServer.getConfig("importer.support_user_id"));
			insert.setString(3, subject);
			insert.setString(4, text);
			insert.executeUpdate();
		} catch (SQLException e) {
			_log.warning("message error: " + e.toString());
		}
	}
	*/
}
