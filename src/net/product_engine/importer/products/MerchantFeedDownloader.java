package net.product_engine.importer.products;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import net.product_engine.models.Merchant;

public class MerchantFeedDownloader extends Thread {
	protected static final Logger _log = Logger.getLogger("sh.save_ca.importer");
	private static MerchantFeedDownloader _instance = null;
	private AtomicBoolean _active = new AtomicBoolean(true);
	private AtomicBoolean _paused = new AtomicBoolean(false);
	private ConcurrentLinkedQueue<Merchant> _queue = new ConcurrentLinkedQueue<Merchant>();
	public static AtomicInteger active_downloads = new AtomicInteger(0);
	public static AtomicInteger left_to_process = new AtomicInteger(0);
	public static int max_active_downloads = 2;
	
	public MerchantFeedDownloader() {
		
	}
	
	public static MerchantFeedDownloader singleton() {
		if (_instance == null) {
			_instance = new MerchantFeedDownloader();
			_instance.start();
		}
		
		return _instance;
	}
	public void add(Merchant m) {
		_queue.add(m);
		left_to_process.incrementAndGet();
		force();
	}
	
	public void run() {
		while (_active.get()) {
			if (_queue.size() > 0) {
				if (active_downloads.get() < max_active_downloads) {
					final Merchant m = _queue.poll();
					(new Thread () {
						public void run() {
							_log.fine("creating file for merchant " + m.id);
							MerchantFeedDownloader.active_downloads.incrementAndGet();
							try {
								m.downloadFeed();
								Server.singleton().addMerchant(m);
							} catch (Exception e) {
								_log.info("merchant " + m.id + ": download failed (" + e.toString() + ")");
								try {
									m.updateProductProcessingInfo("feed download failed", true);
								} catch (SQLException e1) { }
								_send_email(m, e.toString());
								MerchantFeedDownloader.left_to_process.decrementAndGet();
							} finally {
								_log.fine("closing file for merchant " + m.id);
								MerchantFeedDownloader.active_downloads.decrementAndGet();
							}
						}
					}).start();
				} else {
					try {
						Thread.sleep(15000);
					} catch (InterruptedException e) {
						_log.warning(e.toString());
						break;
					}
				}
			} else {
				synchronized (this) {
					try {
						_paused.set(true);
						_log.finer("no merchants. waiting ..");
						wait();
					} catch (InterruptedException e) {
						_log.warning("no merchants wait failed (" + e.toString() + ")");
					//	_active.set(false);
					}
				}
			}
		}
	}
	public synchronized void force() {
		if (_paused.get()) {
			_paused.set(false);
			_log.info("forced");
			notify();
		}
	}
	
	public int getQueueSize() {
		return _queue.size();
	}
	public HashSet<Integer> getQueuedMerchantIds() {
		HashSet<Integer> result = new HashSet<Integer>();
		Iterator<Merchant> merchants = _queue.iterator();
		while(merchants.hasNext()) {
			result.add(merchants.next().id);
		}
		return result;
	}
	
	private void _send_email(Merchant m, String message) {
		//TODO: send message
	}
}
