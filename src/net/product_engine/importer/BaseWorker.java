package net.product_engine.importer;

import java.util.Random;
import java.util.logging.Logger;

import net.product_engine.models.Merchant;


public class BaseWorker extends Thread {
	protected static final Logger _log = Logger.getLogger(BaseServer.namespace);
	protected int _id;
	protected Merchant _merchant = null;
	protected boolean _manual_run = false;
	protected int _steps = 0;
	protected int _total_steps = 7;
	protected String _result = "";
	
	public BaseWorker(Merchant merchant) {
		_merchant = merchant;
		_id = (new Random()).nextInt(1000);
	}
	public BaseWorker(Merchant merchant, int id) {
		_merchant = merchant;
		_id = id;
	}
	
	public long getId() {
		return _id;
	}
	public String getFullName() {
		return "worker" + _id;
	}
	public int getProgress() {
		return _total_steps > 0 ? (int)(_steps * 100 / _total_steps) : 0;
	}
	public Merchant getMerchant() {
		return _merchant;
	}
	public int getMerchantId() {
		return _merchant.id;
	}
	public String getResult() {
		return _result;
	}
}
