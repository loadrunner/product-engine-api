package net.product_engine.importer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.product_engine.common.LogItem;
import net.product_engine.common.LogList;
import net.product_engine.models.Merchant;

abstract public class BaseServer extends net.product_engine.common.BaseServer {
	public ConcurrentLinkedQueue<Merchant> merchants = new ConcurrentLinkedQueue<Merchant>();
	public ArrayList<BaseWorker> workers = new ArrayList<BaseWorker>();
	public LogList<LogItem> past_log = new LogList<LogItem>(100);
	
	public BaseServer() {
		super();
		
		createScheduler();
	}
	
	protected boolean _init() {
		if (!init_run) {
			return false;
		}
		
		return true;
	}
	
	public synchronized void pause() {
		try {
			_paused.set(true);
			_log.finer("no merchants. waiting ..");
			wait();
		} catch (InterruptedException e) {
			_log.warning("no merchants wait failed (" + e.toString() + ")");
		//	_active.set(false);
		}
	}
	public synchronized void force() {
		if (_paused.get()) {
			_paused.set(false);
			_log.info("forced");
			notify();
		}
	}
	public int getWorkerQueueSize() {
		return workers.size();
	}
	public int getMerchantQueueSize() {
		return merchants.size();
	}
	public void addMerchant(Merchant merchant) {
		if (!checkMerchantExistsInQueue(merchant.id)) {
			merchants.add(merchant);
			force();
		}
	}
	public boolean checkMerchantExistsInQueue(int id) {
		return getQueuedMerchants().contains(id);
	}
	public HashSet<Integer> getQueuedMerchants() {
		HashSet<Integer> ids = new HashSet<Integer>();
		
		Iterator<Merchant> itr = merchants.iterator();
		while(itr.hasNext()) {
			Merchant m = itr.next();
			ids.add(m.id);
		}
		
		Iterator<BaseWorker> itr2 = workers.iterator();
		while(itr2.hasNext()) {
			BaseWorker w = itr2.next();
			ids.add(w.getMerchantId());
		}
		
		return ids;
	}
	
	public abstract void recharge();
	public abstract void recharge(int n);
	
	protected abstract void createScheduler();
	protected void cleanupWorkerQueue() {
		Iterator<BaseWorker> itr = workers.iterator();
		while(itr.hasNext()) {
			BaseWorker thread = itr.next();
			if (!thread.isAlive()) {
				Merchant m = thread.getMerchant();
				past_log.add(new LogItem(m.id, m.name, thread.getResult()));
				_log.fine("thread finished; deleting");
				itr.remove();
			}
		}
	}
}