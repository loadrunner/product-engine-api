package net.product_engine.common;

import java.util.concurrent.ConcurrentLinkedQueue;

@SuppressWarnings("hiding")
public class LogList<LogItem> extends ConcurrentLinkedQueue<LogItem> {
	private int _max_size = 0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 5557227802123742733L;
	
	public LogList(int n) {
		_max_size = n;
	}
	
	@Override
	public boolean add(LogItem e) {
		if (_max_size > 0 && size() >= _max_size) {
			for (int i = 0; i <= (size() - _max_size); i++) {
				poll();
			}
		}
		
		return super.add(e);
	}
}
