package net.product_engine.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Pattern;

public class Util {
	public static void copyPipe(InputStream in, OutputStream out, int bufSizeHint) throws IOException {
		int read = -1;
		byte[] buf = new byte[bufSizeHint];
		while ((read = in.read(buf, 0, bufSizeHint)) >= 0) {
			out.write(buf, 0, read);
		}
		out.flush();
	}
	public static final String slug(String input) {
		if (input == null) {
			return "";
		}
		
	//	$sir = str_replace(array('ş', 'Ş', 'ă', 'Ă', 'ţ', 'Ţ', 'î', 'Î', 'â', 'Â'), array('s', 'S', 'a', 'A', 't', 'T', 'i', 'I', 'a', 'A'), trim($string));
	//	$sir = preg_replace('/[^a-zA-Z0-9\-]/', '-', $sir);
	//	$sir = preg_replace('!-+!', '-', $sir);
		
		String nfdNormalizedString = Normalizer.normalize(input, Normalizer.Form.NFD); 
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return
			pattern.matcher(nfdNormalizedString).replaceAll("")
				.replaceAll("[^a-zA-Z0-9\\-]", "-")
				.replaceAll("!-+!", "-");
	}
	public static String toString(HashSet<Integer> list) {
		String result = "";
		Iterator<Integer> itr = list.iterator();
		while(itr.hasNext()) {
			if (result.length() > 0) {
				result += ",";
			}
			result += itr.next().toString();
		}
		return result;
	}
	public static final String computeSum(String input) {
		if (input == null) {
			throw new IllegalArgumentException("Input cannot be null!");
		}
		
		StringBuffer sbuf = new StringBuffer();
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			BaseServer._log.warning("md5 algorithm not found");
			return "";
		}
		byte [] raw = md.digest(input.getBytes());
		
		for (int i = 0; i < raw.length; i++) {
			int c = (int) raw[i];
			if (c < 0) {
				c = (Math.abs(c) - 1) ^ 255;
			}
			String block = toHex(c >>> 4) + toHex(c & 15);
			sbuf.append(block);
		}
		
		return sbuf.toString();
	}
	private static final String toHex(int s) {
		if (s < 10) {
		return new StringBuffer().append((char)('0' + s)).toString();
		} else {
			return new StringBuffer().append((char)('A' + (s - 10))).toString();
		}
	}
}
