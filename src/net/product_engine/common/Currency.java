package net.product_engine.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.logging.Logger;

import net.product_engine.importer.products.Server;
import net.product_engine.sql.MysqlConnection;

public class Currency {
	protected static final Logger _log = Logger.getLogger("net.product-engine.common.currency");
	protected static HashMap<String, Double> _rates = new HashMap<String, Double>() {
		private static final long serialVersionUID = -1463293466509374134L;
	};
	
	public static void init() {
		String q = "SELECT * FROM currency";
		Connection db_link;
		try {
			db_link = MysqlConnection.getConnection("live");
			ResultSet r;
			PreparedStatement s;
			
			s = db_link.prepareStatement(q);
			r = s.executeQuery();
			while (r.next()) {
				_log.fine("found currency " + r.getString("code") + " cu valoarea " + r.getDouble("value"));
				_rates.put(r.getString("code"), r.getDouble("value"));
			}
		} catch (SQLException e) {
			_log.warning("Failed to retrieve currencies. (" + e.getMessage() + ")");
		}
	}
	
	public static double getRate(String from) {
		return getRate(from, Server.default_currency);
	}
	public static double getRate(String from, String to) {
		if (from.equals("EUR")) {
			if (_rates.containsKey(to)) {
				return _rates.get(to);
			}
		} else if (to.equals("EUR")) {
			if (_rates.containsKey(from)) {
				DecimalFormat twoDForm = new DecimalFormat("#.##");
				return Double.valueOf(twoDForm.format(1 / _rates.get(from)));
			}
		} else if (_rates.containsKey(from) && _rates.containsKey(to)) {
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			return Double.valueOf(twoDForm.format(1 / _rates.get(from) * _rates.get(to)));
		}
		
		return 1;
	}
}
