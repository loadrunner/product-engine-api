package net.product_engine.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

abstract public class BaseServer extends Thread {
	public final static String namespace = "net.product_engine";
	public static boolean init_run = false;
	public static Logger _log;
	protected AtomicBoolean _active = new AtomicBoolean(true);
	protected AtomicBoolean _paused = new AtomicBoolean(false);
	private static Properties _config = new Properties();
	
	protected BaseServer() {
		setDaemon(true);
	}
	
	public void run() {
		while (_active.get()) {
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				_log.warning(e.toString());
				break;
			}
		}
		
		_log.info("closing main thread");
	}
	
	public synchronized void pause() {
		try {
			_paused.set(true);
			_log.finer("pausing  ..");
			wait();
		} catch (InterruptedException e) {
			_log.warning("pause failed (" + e.toString() + ")");
		//	_active.set(false);
		}
	}
	public synchronized void force() {
		if (_paused.get()) {
			_paused.set(false);
			_log.info("forced");
			notify();
		}
	}
	
	public static String getConfig(String key) {
		return getConfig(key, false);
	}
	public static String getConfig(String key, boolean absolute) {
		if(!absolute) {
			key = namespace + "." + key;
		}
		return _config.getProperty(key);
	}
	
	public static void init() {
		if (init_run) {
			return;
		}
		
		System.setProperty("java.util.logging.config.file", System.getProperty("catalina.home") + "/conf/logging.properties");
		try {
			LogManager.getLogManager().readConfiguration();
		} catch (SecurityException | IOException e) {
			System.out.println("FATAL EROR: " + e.toString());
			return;
		}
		_log = Logger.getLogger(namespace);
		_log.setLevel(Level.FINEST);
		
	//	_log.severe(System.getProperty("user.dir"));
	//	_log.severe(System.getProperty("java.util.logging.config.file"));
		/*
		_log.severe("doing stucxff");
		_log.warning("doing stucxff");
		_log.info("doing stucxff");
		_log.config("doing stucxff");
		_log.fine("doing stucxff");
		_log.finer("doing stucxff");
		_log.finest("doing stucxff");
		*/
		try {
			_config.load(new FileInputStream(new File(System.getProperty("catalina.home") + "/conf/general.properties")));
		} catch (IOException e) {
			_log.severe("config file not found!" + e.toString());
			return;
		}
		
		init_run = true;
	}
	
	abstract protected boolean _init();
}
