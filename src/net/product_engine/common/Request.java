package net.product_engine.common;

import javax.servlet.http.HttpServletRequest;

public class Request {
	public static String get(HttpServletRequest request, String name, String default_value) {
		String s = request.getParameter(name);
		if (s == null) {
			return default_value;
		} else {
			return s;
		}
	}
}
