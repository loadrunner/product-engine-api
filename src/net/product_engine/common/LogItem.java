package net.product_engine.common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LogItem {
	private int _id_merchant;
	private String _date;
	private String _msg;
	private String _merchant;
	
	public LogItem(int id, String merchant, String msg, String d) {
		_id_merchant = id;
		_merchant = merchant;
		_msg = msg;
		_date = d;
	}
	public LogItem(int id, String merchant, String msg) {
		this(id, merchant, msg, (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).format(new Date()));
	}
	public LogItem(int id, String msg) {
		this(id, null, msg, (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).format(new Date()));
	}
	
	public int getMerchantId() {
		return _id_merchant;
	}
	public String getMerchantName() {
		return _merchant;
	}
	public String getDate() {
		return _date;
	}
	public String getMessage() {
		return _msg;
	}
}
