package net.product_engine.common;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;


public class WebJSONException extends WebApplicationException {
	private static final long serialVersionUID = -4046510049016713362L;
	
	public WebJSONException(int code) {
		super(Response.status(code).entity(constructMessage(code, "")).type(MediaType.APPLICATION_JSON).build());
	}
	public WebJSONException(int code, String message) {
		super(Response.status(code).entity(constructMessage(code, message)).type(MediaType.APPLICATION_JSON).build());
	}
	
	private static String constructMessage(int code, String message) {
		JSONObject r = new JSONObject();
		try {
			r.put("code", code);
			r.put("message", message);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return r.toString();
	}
}