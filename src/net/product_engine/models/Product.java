package net.product_engine.models;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.lucene.document.Document;

@XmlRootElement(name = "Product")
public class Product extends BaseModel {
	/*
	public static HashMap<String, int[]> sizes = new HashMap<String, int[]>() {
		private static final long serialVersionUID = 1L; {
	//	put("icon", new int[] {60, 60});
	//	put("tiny", new int[] {100, 100});
		put("thumb", new int[] {120, 120});
	//	put("small", new int[] {160, 160});
	//	put("medium", new int[] {400, 400});
	//	put("large", new int[] {600, 600});
	}};
	*/
	
	private String _id;
	private String _name;
	
//	private String[] _image_urls = new String[3];
//	public static AtomicInteger active_downloads = new AtomicInteger(0);
//	public static int max_active_downloads = 2;
	
	public Product(Document d) {
		_name = d.get("name");
	}
	
	
	public String getId() {
		return _id;
	}
	public String getName() {
		return _name;
	}
	
	/*
	public String[] getImageUrls() {
		return _image_urls;
	}
	public String getImageUrlsString() {
		String s = "";
		
		if (_image_urls[0] != null && _image_urls[0].length() > 0) {
			s = s.length() > 0 ? ", " + _image_urls[0] : _image_urls[0];
		}
		
		if (_image_urls[1] != null && _image_urls[1].length() > 0) {
			s = s.length() > 0 ? ", " + _image_urls[1] : _image_urls[1];
		}
		
		if (_image_urls[2] != null && _image_urls[2].length() > 0) {
			s = s.length() > 0 ? ", " + _image_urls[2] : _image_urls[2];
		}
		
		return s;
	}
	public Pair<Integer, String> processImages(String merchant_folder) {
		active_downloads.incrementAndGet();
		int count = 0;
		String msg = "null";
		String image_folder = merchant_folder + "/" + ((int)(_id/100)), orig_filename;
		
		if (!(new File(image_folder)).exists()) {
		//	_log.finer("merchantproduct " + _id + ": image folder not found");
			if (!(new File(image_folder)).mkdirs()) {
			//	_log.warning("merchantproduct " + _id + ": cannot create image folder: " + image_folder);
				//recheck, if a different thread already created it
				if (!(new File(image_folder)).exists()) {
					active_downloads.decrementAndGet();
					return new Pair<Integer, String>(0, "cannot create folder");
				}
				
			}
		}
		
		int imported_images = 0, max_mp = -1, max_i = -1, mp;
		int orig_x, orig_y, x, y;
		Info image_info;
		String[] res;
		URI uri;
		
		for (int i = 0; i < _image_urls.length; i++) {
			if (_image_urls[i] != null && _image_urls[i].length() > 15) {
				orig_filename = image_folder + "/original_" + _id + "_" + (i + 1) + ".jpg";
				
				try {
					uri = new URI(_image_urls[i]);
					
					if (uri.isAbsolute() && _downloadOriginalImage(uri, orig_filename)) {
						image_info = new Info(orig_filename);
						res = image_info.getProperty("Geometry").split("(x|\\+)");
						
						orig_x = Integer.parseInt(res[0]);
						orig_y = Integer.parseInt(res[1]);
						mp = Math.round((float)(orig_x * orig_y) * 10 / 1000000);
						
						for (Map.Entry<String, int[]> size : sizes.entrySet()) {
							x = size.getValue()[0];
							y = size.getValue()[1];
							
							// create command
							ConvertCmd cmd = new ConvertCmd();
							
							// create the operation, add images and operators/options
							IMOperation op = new IMOperation();
							op.addImage(orig_filename);
							op.fuzz(5000.0);
							op.trim();
							if (x < orig_x || y < orig_y) {
								op.resize(x,y);
							}
							op.background("white");
							op.gravity("center");
							op.extent(x,y);
							op.quality(90.0);
							op.addImage(image_folder + "/" + _id + "_" + (i + 1) + "_" + size.getKey() + ".jpg");
							
							// execute the operation
							cmd.run(op);
						}
						count++;
						
						if (max_mp < mp) {
							max_mp = mp;
							max_i = i;
						}
						
						imported_images ^= 1 << i;
					}
				} catch (IOException e) {
				//	_log.fine("merchantproduct " + _id + ": cannot download image " + i + " (" + e.toString() + ")");
					msg = "cannot download image";
				} catch (URISyntaxException e) {
				//	_log.fine("merchantproduct " + _id + ": invalid url " + i + " (" + e.toString() + ")");
					msg = "invalid url";
				} catch (NullPointerException | IM4JavaException | InterruptedException e) {
				//	_log.fine("merchantproduct " + _id + ": imagemagick error with image " + i + " (" + e.toString() + ")");
					msg = "imagemagick error";
					try {
						(new File(orig_filename)).delete();
					} catch (Exception ee) {
						
					}
				} catch (Exception e) {
				//	_log.fine("merchantproduct " + _id + ": imagemagick error with image " + i + " (" + e.toString() + ")");
					msg = "unknown error";
					try {
						(new File(orig_filename)).delete();
					} catch (Exception ee) {
						
					}
				}
			}
		}
		
		if (count > 0) {
			msg = "ok" + imported_images;
			
			imported_images ^= (max_i + 1) << 3;
			imported_images ^= Math.min(max_mp, 20) << 5;
			
		//	_updateImportedImages(imported_images);
		}
		
	//	_log.fine("product " + _id + ": download images complete");
		active_downloads.decrementAndGet();
		return new Pair<Integer, String>(count, msg);
	}
	private boolean _downloadOriginalImage(URI src, String dest) throws IOException {
		int bufSize = 8 * 1024;
		
		URL url = src.toURL();
		URLConnection uc = url.openConnection();
		uc.setRequestProperty("User-Agent", "Mozilla 5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.11)");
		uc.setConnectTimeout(10000);
		uc.setReadTimeout(30000);
		File destination = new File(dest);
		
		BufferedInputStream fin = new BufferedInputStream(uc.getInputStream(), bufSize);
		BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(destination), bufSize);
		Util.copyPipe(fin, fout, bufSize);
		fin.close();
		fout.close();
		if (destination.isFile()) {
			BaseServer._log.fine("thread " + _id + ": download file "+ src.toString() + " complete in " + dest);
			return true;
		} else {
			return false;
		}
	}
	*/
}

/*
public class Budget {
	@Id
	private ObjectId _id;
	private ObjectId user_id;
	private String name;
	private Tag tag;
	private double value;
	private String currency;
	private int length;
	private boolean is_hidden;
	private short type;
	@SuppressWarnings("unused")
	@XmlElement(name = "level")
	@NotSaved
	private double level;
	@SuppressWarnings("unused")
	@XmlElement(name = "last_level")
	@NotSaved
	private double last_level;
	@SuppressWarnings("unused")
	@XmlElement(name = "level_avg")
	@NotSaved
	private double level_avg;
	
	public Budget() {
		this(null, null, null, 0, null, 0, (short) (TYPE_DEPOSIT & TYPE_WITHDRAWAL));
	}
	public Budget(ObjectId uid, String name, Tag tag, double value, String currency, int length, short type) {
		setUserId(uid);
		setName(name);
		setTag(tag);
		setValue(value);
		setCurrency(currency);
		setLength(length);
		setType(type);
	}
	
	public ObjectId getId() {
		return _id;
	}
	@XmlElement(name = "id")
	public String getIdSring() {
		return _id.toString();
	}
	public String getName() {
		return name;
	}
	public Tag getTag() {
		return tag;
	}
	public double getValue() {
		return value;
	}
	public String getCurrency() {
		return currency;
	}
	public int getLength() {
		return length;
	}
	public int getStartTimestamp()
	{
		Calendar date = Calendar.getInstance();
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		
		//TODO: use length ..
		if ((type & TYPE_DAILY) > 0) {
			//nothing ..
		} else if ((type & TYPE_WEEKLY) > 0) {
			date.set(Calendar.DAY_OF_WEEK, date.getFirstDayOfWeek());
		} else if ((type & TYPE_MONTHLY) > 0) {
			date.set(Calendar.DAY_OF_MONTH, 1);
		} else if ((type & TYPE_QUARTERLY) > 0) {
			int quarter = date.get(Calendar.MONTH) / 3 + 1;
			date.set(Calendar.DAY_OF_MONTH, 1);
			date.set(Calendar.MONTH, (quarter - 1) * 3);
		} else if ((type & TYPE_HALF_YEARLY) > 0) {
			int half = date.get(Calendar.MONTH) / 6 + 1;
			date.set(Calendar.DAY_OF_MONTH, 1);
			date.set(Calendar.MONTH, (half - 1) * 6);
		} else if ((type & TYPE_YEARLY) > 0) {
			date.set(Calendar.DAY_OF_MONTH, 1);
			date.set(Calendar.MONTH, Calendar.JANUARY);
		}
		
		return (int) (date.getTimeInMillis() / 1000);
	}
	public short getType() {
		return type;
	}
	@XmlTransient
	public ObjectId getUserId() {
		return user_id;
	}
	@XmlElement(name = "is_hidden")
	public boolean getIsHidden() {
		return is_hidden;
	}
	
	public boolean acceptsDeposits() {
		return (type & TYPE_DEPOSIT) > 0;
	}
	public boolean acceptsWithdrawals() {
		return (type & TYPE_WITHDRAWAL) > 0;
	}
	
	public int shiftTimestamp(int timestamp, int amount) {
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis((long) timestamp * 1000); 
		
		if ((type & TYPE_DAILY) > 0) {
			date.add(Calendar.DAY_OF_MONTH, amount);
		} else if ((type & TYPE_WEEKLY) > 0) {
			date.add(Calendar.WEEK_OF_YEAR, amount);
		} else if ((type & TYPE_MONTHLY) > 0) {
			date.add(Calendar.MONTH, amount);
		} else if ((type & TYPE_QUARTERLY) > 0) {
			date.add(Calendar.MONTH, amount * 3);
		} else if ((type & TYPE_HALF_YEARLY) > 0) {
			date.add(Calendar.MONTH, amount * 6);
		} else if ((type & TYPE_YEARLY) > 0) {
			date.add(Calendar.MONTH, amount);
		}
		
		return (int) (date.getTimeInMillis() / 1000);
	}
	public void processData() {
		level = 0;
		last_level = 0;
		level_avg = 0;
		
		try {
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			DBCollection tran = TransactionDAO.singleton().getCollection();
			
			BasicDBObject initial = new BasicDBObject();
			BasicDBObject query = new BasicDBObject();
			query.put("user_id", getUserId());
			query.put("type", (getType() & (TYPE_DEPOSIT | TYPE_WITHDRAWAL)));
			query.put("tags.name", tag.getName());
			
			//current level
			int start = getStartTimestamp();
			BasicDBObject timestampCond = new BasicDBObject();
			timestampCond.put("$gte", start);
			timestampCond.put("$lt", shiftTimestamp(start, length));
			query.put("timestamp", timestampCond);
			
			BasicDBList result = (BasicDBList) tran.group(null, query, initial, "function (o, p) { if (o.currency in p) p[o.currency] += o.value; else p[o.currency] = o.value; }");
			
			if (result.size() > 0) {
				level = sumCurrencies((DBObject) result.get(0), currency);
			}
			
			//last level
			timestampCond = new BasicDBObject();
			timestampCond.put("$gte", shiftTimestamp(start, -length));
			timestampCond.put("$lt", start);
			query.put("timestamp", timestampCond);
			
			result = (BasicDBList) tran.group(null, query, initial, "function (o, p) { if (o.currency in p) p[o.currency] += o.value; else p[o.currency] = o.value; }");
			
			if (result.size() > 0) {
				last_level = sumCurrencies((DBObject) result.get(0), currency);
			}
			
			//level prev avg
			int avgLength = 6;
			timestampCond = new BasicDBObject();
			timestampCond.put("$gte", shiftTimestamp(start, -length * avgLength));
			timestampCond.put("$lt", start);
			query.put("timestamp", timestampCond);
			
			result = (BasicDBList) tran.group(null, query, initial, "function (o, p) { if (o.currency in p) p[o.currency] += o.value; else p[o.currency] = o.value; }");
			
			if (result.size() > 0) {
				try {
					level_avg = Double.valueOf(twoDForm.format(sumCurrencies((DBObject) result.get(0), currency) / avgLength));
				} catch (Exception e) { }
			}
		} catch (Exception e) {
			System.out.println("3.2.4: " + e.toString());
		}
	}
	
	public void setName(String value) {
		if (validateName(value)) {
			name = value;
		}
	}
	public void setTag(Tag value) {
		tag = value;
	}
	public void setValue(double value) {
		if (validateValue(value)) {
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			this.value = Double.valueOf(twoDForm.format(value));
		}
	}
	public void setCurrency(String value) {
		if (validateCurrency(value)) {
			currency = value;
		}
	}
	public void setLength(int value) {
		if (validateLength(value)) {
			length = value;
		}
	}
	public void setType(short value) {
		if (validateType(value)) {
			type = value;
		}
	}
	public void setUserId(String value) {
		user_id = new ObjectId(value);
	}
	public void setUserId(ObjectId value) {
		user_id = value;
	}
	public void setHidden(boolean value) {
		is_hidden = value;
	}
	
	public static boolean validateName(String value) {
		return value != null
			&& value.length() >= 2 && value.length() <= 24;
	}
	public static boolean validateValue(double value) {
		return value > 0 && value < 1000000000d;
	}
	public static boolean validateLength(int value) {
		return value > 0 && value < 365;
	}
	public static boolean validateCurrency(String value) {
		try {
			if (CurrencyDAO.singleton().findOne(value) != null) {
				return true;
			}
		} catch (Exception e) { }
		
		return false;
	}
	public static boolean validateType(short value) {
		return ((value & TYPE_DEPOSIT) > 0 || (value & TYPE_WITHDRAWAL) > 0)
		 && (
		    (value & TYPE_DAILY) > 0
		 || (value & TYPE_WEEKLY) > 0
		 || (value & TYPE_MONTHLY) > 0
		 || (value & TYPE_QUARTERLY) > 0
		 || (value & TYPE_HALF_YEARLY) > 0
		 || (value & TYPE_YEARLY) > 0
		);
	}
	
	private static double sumCurrencies(DBObject r, String defaultCurrency) {
		double sum = 0;
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		
		Set<String> keys = r.keySet();
		Iterator<String> it = keys.iterator();
		
		while (it.hasNext()) {
			String c = it.next();
			try {
				double b = Double.valueOf(twoDForm.format((double) r.get(c)));
				if (c == defaultCurrency) {
					sum += b;
				} else {
					sum += b * Currency.getRate(c, defaultCurrency);
				}
			} catch (Exception e) { }
		}
		return sum;
	}
}
*/