package net.product_engine.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

import net.product_engine.importer.products.filetypes.*;
import net.product_engine.sql.MysqlConnection;


public class Merchant {
	protected static final Logger _log = Logger.getLogger("sh.save_ca.importer");
	private BaseFile _file;
	public int id = 0;
	public String url_import = null;
	public String file_type = null;
	public String field_corelation = null;
	public char csv_field_separator;
	public String name = "unknown";
	public boolean vat_included;
	public String currency;
	
	public Merchant(int id_, String name_, String url_, String type_, char separator, String field_corelation_, boolean vat_included_, String currency_) {
		id = id_;
		name = name_;
		url_import = url_;
		file_type = type_;
		csv_field_separator = separator;
		field_corelation = field_corelation_;
		vat_included = vat_included_;
		currency = currency_ != null ? currency_.toUpperCase() : "RON";
	}
	public Merchant(int id, String name) {
		this(id, name, null, null, ',', null, false, null);
	}
	public Merchant(int id, String url, String type) {
		this(id, null, url, type, ',', null, true, "RON");
	}
	public Merchant(int id, String url, String type, boolean vat_included) {
		this(id, null, url, type, ',', null, vat_included, "RON");
	}
	
	public void downloadFeed() throws Exception {
		if (file_type.equals("csv_2parale")) {
			_file = new DouaParaleCsvFile(id, url_import, field_corelation, csv_field_separator);
		} else if (file_type.equals("xml")) {
			_file = new XmlFile(id, url_import, field_corelation);
		} else if (file_type.equals("xml_invalid")) {
			_file = new InvalidXmlFile(id, url_import, field_corelation);
		} else if (file_type.equals("xml_shopmania")) {
			_file = new ShopmaniaXmlFile(id, url_import, field_corelation);
		} else if (file_type.equals("csv")) {
			_file = new CsvFile(id, url_import, field_corelation, csv_field_separator);
		} else if (file_type.equals("csv_invalid")) {
			_file = new InvalidCsvFile(id, url_import, field_corelation, csv_field_separator);
		} else {
			throw new Exception("Invalid file type '" + file_type + "'!");
		}
	}
	public BaseFile getFile() throws Exception {
		_file.init();
		return _file;
	}
	public void updateProductProcessingInfo(String msg, boolean fail) throws SQLException {
		Connection db_link = MysqlConnection.getConnection("live");
		String q;
		
		q = "UPDATE merchant SET modified_at = NOW(), product_feed_processed_at = NOW(), product_feed_process_message = ? " + 
		(fail ? ", product_feed_errors = product_feed_errors + 1 " : "") + 
		"WHERE id = ?;";
		
		PreparedStatement update = db_link.prepareStatement(q);
		update.setString(1, msg);
		update.setInt(2, id);
		update.executeUpdate();
	}
	public void updateImagesProcessingDate() throws SQLException {
		Connection db_link = MysqlConnection.getConnection("live");
		String q;
		
		q = "UPDATE merchant SET modified_at = NOW(), product_images_downloaded_at = NOW() WHERE id = ?;";
		
		PreparedStatement update = db_link.prepareStatement(q);
		update.setInt(1, id);
		update.executeUpdate();
	}
	public void updateOfferDate() throws SQLException {
		Connection db_link = MysqlConnection.getConnection("live");
		String q = "UPDATE merchant SET modified_at = NOW(), product_feed_updated_at = NOW(), product_feed_errors = 0 WHERE id = ?;";
		
		PreparedStatement update = db_link.prepareStatement(q);
		update.setInt(1, id);
		update.executeUpdate();
	}
	public void updateFieldCorelation(String fc) throws SQLException {
		Connection db_link = MysqlConnection.getConnection("live");
		String q = "UPDATE merchant SET modified_at = NOW(), product_feed_field_corelation = ? WHERE id = ?;";
		
		PreparedStatement update = db_link.prepareStatement(q);
		update.setString(1, fc);
		update.setInt(2, id);
		update.executeUpdate();
	}
}
