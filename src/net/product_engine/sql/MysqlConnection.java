package net.product_engine.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import net.product_engine.common.BaseServer;

public class MysqlConnection {
	protected static final Logger _log = Logger.getLogger(BaseServer.namespace + ".importer.sql");
	private static Map<String, MysqlConnection> _instances = new HashMap<String, MysqlConnection>();
	private java.sql.Connection _link = null;
	private final String url = "jdbc:mysql://";
	private String serverName= "localhost";
	private String portNumber = "1433";
	private String databaseName= "pubs";
	private String userName = "user";
	private String password = "password";
	
	// Constructor
	public MysqlConnection(String hostname, String port, String db, String user, String passwd){
		this.serverName = hostname;
		this.portNumber = port;
		this.databaseName = db;
		this.userName = user;
		this.password = passwd;
	}
	
	public static Connection getConnection(String key) throws SQLException {
		return getConnection(key, true);
	}
	public static Connection getConnection(String key, boolean old) throws SQLException {
		if (old && _instances.containsKey(key)) {
			return _instances.get(key).getConnection();
		}
		
		MysqlConnection link = new MysqlConnection(
			BaseServer.getConfig("importer.sql."+key+".host"),
			BaseServer.getConfig("importer.sql."+key+".port"),
			BaseServer.getConfig("importer.sql."+key+".db"),
			BaseServer.getConfig("importer.sql."+key+".username"),
			BaseServer.getConfig("importer.sql."+key+".password")
		);
		_instances.put(key, (MysqlConnection) link);
		
		return link.getConnection();
	}
	
	private String getConnectionUrl(){
		return url+serverName+":"+portNumber+"/"+databaseName+"?user="+userName+"&password="+password+"&useUnicode=true&characterEncoding=UTF8";
	}
	
	public Connection getConnection() {
		if (_link == null) {
			return _connect();
		} else {
			try {
				if (!_link.isClosed()) {
					return _link;
				}
			} catch (SQLException e) {}
			
			_link = null;
			return _connect();
		}
	}
	private Connection _connect() { 
		java.sql.Connection con = null;
		try{
			Class.forName("com.mysql.jdbc.Driver"); 
			con = java.sql.DriverManager.getConnection(getConnectionUrl());
			if (con != null) {
				_log.fine("Connection Successful!");
			}
		}catch(Exception e){
			e.printStackTrace();
			_log.warning("Error Trace in getConnection() : " + e.getMessage());
		}
		
		this._link = con;
		
		return con;
	}

	/*
	Display the driver properties, database details 
	*/

	public void displayDbProperties() {
		java.sql.DatabaseMetaData dm = null;
	//	java.sql.ResultSet rs = null;
		try {
			java.sql.Connection con= this.getConnection();
			if(con != null) {
					dm = con.getMetaData();
					_log.finest("Driver Information");
					_log.finest("\tDriver Name: "+ dm.getDriverName());
					_log.finest("\tDriver Version: "+ dm.getDriverVersion ());
					_log.finest("\nDatabase Information ");
					_log.finest("\tDatabase Name: "+ dm.getDatabaseProductName());
					_log.finest("\tDatabase Version: "+ dm.getDatabaseProductVersion());
				//	_log.finest("Avalilable Catalogs ");
				//	rs = dm.getCatalogs();
				//	while(rs.next()){
				//		_log.finer("\tcatalog: "+ rs.getString(1));
				//	} 
				//	rs.close();
				//	rs = null;
					closeConnection();
			} else {
				_log.warning("Error: No active Connection");
			}
		} catch(Exception e) {
			_log.warning(e.toString());
		}
		dm = null;
	}
	
	public void select(String query) {
		java.sql.ResultSet rs = null;
		java.sql.Connection link = null;
		Statement statement = null;
		
		try{
			link = this.getConnection();
			if (link != null) {
					_log.finest("Query result");
					statement = link.createStatement();
					
					rs = statement.executeQuery(query); 
					
					while (rs.next()) {
						_log.finest("\tuser: "+  rs.getString("ID_Utilizator"));
						_log.finest("\tuser: "+  rs.toString());
					} 
					rs.close();
					rs = null;
			} else {
				_log.warning("Error: No active Connection");
			}
		}catch(Exception e) {
			_log.warning(e.toString());
		}
	}
	public int insert(String query) {
		_log.finest("Query: " + query);
		
		java.sql.Connection link = this.getConnection();
		Statement statement = null;
		int result = 0;
		
		try{
			if (link != null) {
					_log.finest("Query result");
					statement = link.createStatement();
					
					result = statement.executeUpdate(query);
					if (result > 0) {
						_log.finest("insert ok");
					} else {
						_log.finest("insert failed");
					} 
			} else {
				_log.warning("Error: No active Connection");
			}
		}catch(Exception e) {
			_log.warning(e.toString());
		}
		return result;
	}
	public void closeConnection() {
		java.sql.Connection con = this.getConnection();
		try{
			if (con != null) {
					con.close();
			}
			con = null;
		} catch(Exception e) {
		//	e.printStackTrace();
		}
	}
}
