package net.product_engine.api;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import net.product_engine.common.LogItem;
import net.product_engine.common.WebJSONException;
import net.product_engine.importer.BaseWorker;
import net.product_engine.importer.products.MerchantFeedDownloader;
import net.product_engine.importer.products.Server;
import net.product_engine.models.Merchant;
import net.product_engine.sql.MysqlConnection;

import org.json.JSONException;

@Path("/importer/product")
public class ProductImporter {
	@Context
	UriInfo uriInfo;
	@Context
	HttpServletRequest request;
	
	public void init() {
		System.out.println("asdas");
	}
	
	@GET
	@Produces({MediaType.APPLICATION_XML})
	public Document status(
		@QueryParam("query") @DefaultValue("*") String queryString,
		@Context HttpServletResponse response
	) throws IOException, JSONException {
		Server server = Server.singleton();
		if(!server.isAlive()) {
			throw new WebJSONException(500, "Server is not alive!");
		}
		
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			response.setStatus(500);
			return null;
		}
		Document out = docBuilder.newDocument();
		
		Element root = out.createElement("response");
		out.appendChild(root);
		
		Element present = out.createElement("present");
		root.appendChild(present);
		
		Iterator<BaseWorker> workers = server.workers.iterator();
		while(workers.hasNext()) {
			BaseWorker worker = workers.next();
			
			Merchant m = worker.getMerchant();
			
			Element id = out.createElement("id");
			id.appendChild(out.createTextNode(Integer.toString(m.id)));
			
			Element name = out.createElement("name");
			name.appendChild(out.createTextNode(m.name));
			
			Element progress = out.createElement("progress");
			progress.appendChild(out.createTextNode(Integer.toString(worker.getProgress()) + "%"));
			
			Element em = out.createElement("worker" + worker.getId());
			em.appendChild(id);
			em.appendChild(name);
			em.appendChild(progress);
			
			present.appendChild(em);
		}
		
		
		Element future = out.createElement("future");
		root.appendChild(future);
		
		Element queued = out.createElement("queued");
		queued.appendChild(out.createTextNode(Integer.toString(MerchantFeedDownloader.singleton().getQueueSize())));
		future.appendChild(queued);
		
		Element downloading = out.createElement("downloading");
		downloading.appendChild(out.createTextNode(Integer.toString(MerchantFeedDownloader.active_downloads.get())));
		future.appendChild(downloading);
		
		Element downloaded = out.createElement("downloaded");
		future.appendChild(downloaded);
		
		Element d_count = out.createElement("count");
		d_count.appendChild(out.createTextNode(Integer.toString(server.merchants.size())));
		downloaded.appendChild(d_count);
		
		Element d_list = out.createElement("list");
		downloaded.appendChild(d_list);
		
		Iterator<Merchant> merchants = server.merchants.iterator();
		while(merchants.hasNext()) {
			Merchant merchant = merchants.next();
			
			Element id = out.createElement("id");
			id.appendChild(out.createTextNode(Integer.toString(merchant.id)));
			
			Element name = out.createElement("name");
			name.appendChild(out.createTextNode(merchant.name));
			
			Element em = out.createElement("merchant");
			em.appendChild(id);
			em.appendChild(name);
			
			d_list.appendChild(em);
		}
		
		
		Element past = out.createElement("past");
		root.appendChild(past);
		
		Iterator<LogItem> logs = server.past_log.iterator();
		while(logs.hasNext()) {
			LogItem log = logs.next();
			
			Element id = out.createElement("id");
			id.appendChild(out.createTextNode(Integer.toString(log.getMerchantId())));
			
			Element merchant = out.createElement("merchant");
			merchant.appendChild(out.createTextNode(log.getMerchantName()));
			
			Element date = out.createElement("date");
			date.appendChild(out.createTextNode(log.getDate()));
			
			Element message = out.createElement("message");
			message.appendChild(out.createTextNode(log.getMessage()));
			
			Element em = out.createElement("log");
			em.appendChild(id);
			em.appendChild(merchant);
			em.appendChild(date);
			em.appendChild(message);
			
		//	past.appendChild(em);
			past.insertBefore(em, past.getFirstChild());
		}
		
		return out;
	}
	
	@GET
	@Path("recharge")
	@Produces({MediaType.APPLICATION_JSON})
	public String recharge(
		@QueryParam("nr") @DefaultValue("0") int nr,
		@Context HttpServletResponse response
	) throws IOException, JSONException {
		Server server = Server.singleton();
		if(!server.isAlive()) {
			throw new WebJSONException(500, "Server is not alive!");
		}
		
		server.recharge(nr);
		
		return "done " + server.workers.size();
	}
	
	@GET
	@Path("add")
	@Produces({MediaType.APPLICATION_JSON})
	public String add(
		@QueryParam("id") @DefaultValue("-1") int merchantId,
		@Context HttpServletResponse response
	) throws IOException, JSONException {
		Server server = Server.singleton();
		if(!server.isAlive()) {
			throw new WebJSONException(500, "Server is not alive!");
		}
		
		if(merchantId==-1) {
			throw new WebJSONException(400, "Invalid merchant id!");
		}
		
		if (server.checkMerchantExistsInQueue(merchantId)) {
			throw new WebJSONException(400, "Merchant already exists in queue!");
		}
		
		try {
			Connection db_link = MysqlConnection.getConnection("live");
			ResultSet r;
			PreparedStatement s;
			
			s = db_link.prepareStatement(
				"SELECT * FROM merchant m WHERE id = ? AND status = 1 LIMIT 0, 1");
			s.setInt(1, merchantId);
			
			r = s.executeQuery();
			if (r.next()) {
				String host = r.getString("name");
				String url = r.getString("product_feed_url");
				String file_type = r.getString("product_feed_type");
				String sep = r.getString("product_feed_csv_separator");
				char field_separator = sep != null && sep.length() > 0 ? sep.charAt(0) : ',';
				String field_corelation = r.getString("product_feed_field_corelation");
				boolean vat_included = r.getString("product_feed_vat_included").equals("1") ? true : false;
				String currency = r.getString("currency");
				
				Merchant m = new Merchant(merchantId, host, url, file_type, field_separator, field_corelation, vat_included, currency);
				(MerchantFeedDownloader.singleton()).add(m);
			//	server.merchants.add(m);
			//	server.force();
			//	Worker thread = new Worker(m);
			//	thread.run();
				return "merchant " + merchantId + " added to queue";
			} else {
				throw new WebJSONException(400, "Merchant not found or not active!");
			}
		} catch (Exception e) {
			throw new WebJSONException(500, "Cannot get merchant info!");
		}
	}
}