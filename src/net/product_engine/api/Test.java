package net.product_engine.api;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import net.product_engine.LuceneServer;
import net.product_engine.common.WebJSONException;
import net.product_engine.importer.products.Worker;
import net.product_engine.models.Merchant;
import net.product_engine.sql.MysqlConnection;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.facet.search.FacetsCollector;
import org.apache.lucene.facet.search.params.CountFacetRequest;
import org.apache.lucene.facet.search.params.FacetSearchParams;
import org.apache.lucene.facet.search.results.FacetResult;
import org.apache.lucene.facet.search.results.FacetResultNode;
import org.apache.lucene.facet.taxonomy.CategoryPath;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiCollector;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.util.Version;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/test")
public class Test {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public String test(
		@QueryParam("query") @DefaultValue("*") String queryString,
		@Context HttpServletResponse response
	) throws IOException, ParseException, JSONException {
		//String querystr = args.length > 0 ? args[0] : "lucene";
		long timestamp = System.currentTimeMillis();
		LuceneServer server = LuceneServer.singleton();
		
		int[] merchants = {181, 71, 75, 66, 53, 58, 47, 39, 31, 26, 156, 149, 151, 144, 132, 138, 147, 142, 146, 140, 134};
		
		// the "title" arg specifies the default field to use
		// when no field is explicitly specified in the query.
		String[] fields = {"name", "brand", "category", "description"};
		HashMap<String,Float> boosts = new HashMap<String,Float>();
		boosts.put("name", (float) 1.8);
		boosts.put("brand", (float) 1.2);
		boosts.put("category", (float) 1.1);
		boosts.put("description", (float) 0.5);
		
		queryString = queryString.replaceAll("[^\\p{L}\\p{N}]", " ");
		queryString = queryString.replaceAll("[ ]{2,}"," ");
		
		MultiFieldQueryParser qp = new MultiFieldQueryParser(Version.LUCENE_40, fields, server.getAnalizer(), boosts);
		qp.setDefaultOperator(Operator.OR);
		Query q1 = qp.parse(queryString);
		BooleanQuery q2 = new BooleanQuery();
		Random r = new Random();
		for (int i = 0; i < merchants.length; i++) {
			if (r.nextInt() % 2 == 0) {
				continue;
			}
			q2.add(new TermQuery(new Term("merchant_id", Integer.toString(merchants[i]))), BooleanClause.Occur.SHOULD);
		}
		BooleanQuery q = new BooleanQuery();
		q.add(q1, BooleanClause.Occur.MUST);
		//TODO: check out/test filters (CachingWrapperFilter)
		q.add(q2, BooleanClause.Occur.MUST);
		
		// 3. search
		int hitsPerPage = 10;
		IndexSearcher searcher = server.getSearcher();
		TaxonomyReader taxo = server.getTaxoReader();
		TopScoreDocCollector topDocsCollector = TopScoreDocCollector.create(hitsPerPage, true);
		
		FacetSearchParams facetSearchParams = new FacetSearchParams();
		facetSearchParams.addFacetRequest(new CountFacetRequest(new CategoryPath("brand"), 10));
		facetSearchParams.addFacetRequest(new CountFacetRequest(new CategoryPath("category"), 10));
		facetSearchParams.addFacetRequest(new CountFacetRequest(new CategoryPath("merchant_id"), 10));
		
		FacetsCollector facetsCollector = new FacetsCollector(facetSearchParams, searcher.getIndexReader(), taxo);
		
		searcher.search(q, MultiCollector.wrap(topDocsCollector, facetsCollector));
		
		ScoreDoc[] hits = topDocsCollector.topDocs().scoreDocs;
		List<FacetResult> facets = facetsCollector.getFacetResults();
		
		System.out.println("Found " + hits.length + " hits.");
		System.out.println(searcher.collectionStatistics("category").toString());
		JSONObject result = new JSONObject();
		
		DecimalFormat twoDForm = new DecimalFormat("0.000");
		String time = twoDForm.format((double) (System.currentTimeMillis() - timestamp) / 1000L);
		
		// 4. display results
		System.out.println("Selected " + hits.length + " documents for query \"" + queryString + "\" in " + time + " sec.");
		String output;
		for(int i=0; i < Math.min(5, hits.length); i++) {
		//	if (searcher.isDeleted(i))
		//		continue;
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			List<IndexableField> l = d.getFields();
			if (l.size() > 0) {
				output = "";
				for (int j = 0; j < l.size(); j++) {
					output += ",##[" + l.get(j).name() + "] -- [" + d.get(l.get(j).name()) + "]##";
				}
				output += " ##score - " + hits[i].score;
			} else {
				output = "no fields .. sad";
			}
			
			System.out.println((i + 1) + ". " + output);
			result.put(Integer.toString(i + 1), output);
		}
		
		//System.out.println("facet dump " + facets.toString());
		for (FacetResult facet : facets) {
			FacetResultNode node = facet.getFacetResultNode();
			String res = "";
			if (node.getNumSubResults() > 0) {
				for (FacetResultNode subnode : node.getSubResults()) {
					res += subnode.getLabel() + " (" + node.getValue() + "), ";
				}
				
			} else {
				res = "no subnodes";
			}
			System.out.println("facet node: " + node.getLabel() + " - " + res);
		}
		
		return result.toString();
	}
	
	@GET
	@Path("create")
	@Produces({MediaType.APPLICATION_JSON})
	public String createIndex(
			@Context HttpServletResponse response,
			@QueryParam("clear") @DefaultValue("false") Boolean clearIndex
		) throws IOException {
		
		LuceneServer server = LuceneServer.singleton();
		
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, server.getAnalizer());
		
		if (clearIndex) {
			// Create a new index in the directory, removing any
			// previously indexed documents:
			config.setOpenMode(OpenMode.CREATE);
		} else {
			// Add new documents to an existing index:
			config.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}
		
		IndexWriter w = server.getIndexWriter();
		addDocument(w, "lucene in Action", 23000000, 23000000);
		addDocument(w, "lucene for Dummies", 23000000, 23000000);
		addDocument(w, "Managing Gigabytes", 23000000, 23000000);
		addDocument(w, "The Art of Computer Science", 23000000, 23000000);
		w.close();
		
		return server.getIndex().toString();
	}
	
	@GET
	@Path("import")
	@Produces({MediaType.APPLICATION_JSON})
	public String importMerchant(
			@QueryParam("id") @DefaultValue("0") int merchant_id,
			@QueryParam("clear") @DefaultValue("false") Boolean clearIndex,
			@Context HttpServletResponse response) throws IOException {
		
		if (merchant_id <= 0) {
			throw new WebJSONException(400, "Invalid merchant_id");
		}
		
		try {
			Connection db_link = MysqlConnection.getConnection("live");
			ResultSet r;
			PreparedStatement s;
			
			s = db_link.prepareStatement("SELECT m.* FROM merchant m WHERE m.id = ? LIMIT 0, 1");
			s.setInt(1, merchant_id);
			
			r = s.executeQuery();
			if (r.next()) {
				String host = r.getString("name");
				String url = r.getString("product_feed_url");
				String file_type = r.getString("product_feed_type");
				String sep = r.getString("product_feed_csv_separator");
				char field_separator = sep != null && sep.length() > 0 ? sep.charAt(0) : ',';
				String field_corelation = r.getString("product_feed_field_corelation");
				boolean vat_included = r.getString("product_feed_vat_included").equals("1") ? true : false;
				String currency = r.getString("currency");
				
				Merchant merchant = new Merchant(merchant_id, host, url, file_type, field_separator, field_corelation, vat_included, currency);
				IndexWriter w = null;
				try {
					merchant.downloadFeed();
					
					LuceneServer server = LuceneServer.singleton();
					
					IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, server.getAnalizer());
					
					if (clearIndex) {
						// Create a new index in the directory, removing any
						// previously indexed documents:
						config.setOpenMode(OpenMode.CREATE);
					} else {
						// Add new documents to an existing index:
						config.setOpenMode(OpenMode.CREATE_OR_APPEND);
					}
					
					w = server.getIndexWriter();
					
					Worker thread = new Worker(merchant, w, server.getTaxoWriter());
					thread.start();
					
					return "Fisierul a fost downloadat si va fi procesat in cel mai scurt timp.\nVa multumim.";
				} catch (Exception e) {
					w.rollback();
					w.close();
				//	_log.info("merchant " + id + ": download failed (" + e.toString() + ")");
					throw new WebJSONException(500, "Nu s-a putut downloada fisierul.\nMesaj eroare: " + e.toString());
				}
			} else {
			//	_log.info("merchant not found");
				throw new WebJSONException(400, "merchant_id not found");
			}
		} catch (SQLException e) {
		//	_log.info("cannot get merchant! (" + e.toString() + ")");
			e.printStackTrace();
			throw new WebJSONException(500, "Internal error");
		}
	}
	
	private static void addDocument(IndexWriter w, String name, int lat, int lng) throws IOException {
		Document doc = new Document();
		doc.add(new Field("name", name, TextField.TYPE_STORED));
	//	doc.add(new );
		w.addDocument(doc);
	}
}