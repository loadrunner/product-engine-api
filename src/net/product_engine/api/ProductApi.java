package net.product_engine.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import net.product_engine.LuceneServer;
import net.product_engine.common.WebJSONException;

import org.apache.lucene.document.Document;
import org.apache.lucene.facet.search.FacetsCollector;
import org.apache.lucene.facet.search.params.CountFacetRequest;
import org.apache.lucene.facet.search.params.FacetSearchParams;
import org.apache.lucene.facet.search.results.FacetResult;
import org.apache.lucene.facet.search.results.FacetResultNode;
import org.apache.lucene.facet.taxonomy.CategoryPath;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.CachingWrapperFilter;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiCollector;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.TotalHitCountCollector;
import org.apache.lucene.util.Version;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/product")
public class ProductApi {
	@Context
	UriInfo uriInfo;
	@Context
	HttpServletRequest request;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public String get(
		@QueryParam("query") @DefaultValue("*") String queryString,
		@QueryParam("merchant_ids") @DefaultValue("*") String merchantIds,
		@QueryParam("price_min") @DefaultValue("-1") int priceMin,
		@QueryParam("price_max") @DefaultValue("-1") int priceMax,
		@QueryParam("offset") @DefaultValue("0") int offset,
		@QueryParam("limit") @DefaultValue("10") int limit,
		@Context HttpServletResponse response
	) throws IOException, JSONException, ParseException {
	//	long timestamp = System.currentTimeMillis();
		LuceneServer server = LuceneServer.singleton();
		if(!server.isAlive()) {
			throw new WebJSONException(500, "Server is not alive!");
		}
		
		ArrayList<Integer> merchants = new ArrayList<Integer>();
		
		//{181,71,75,66,53,58,47,39,31,26,156,149,151,144,132,138,147,142,146,140,134}
		String[] ids = merchantIds.split(",");
		for (int i = 0; i < ids.length; i++) {
			try {
				merchants.add(Integer.parseInt(ids[i].trim()));
			} catch (NumberFormatException e) { }
		}
		
		offset = Math.min(1000,Math.max(0,offset));
		limit = Math.min(100,Math.max(1,limit));
		
		BooleanQuery q2 = new BooleanQuery();
		for (Integer merchant_id : merchants) {
			q2.add(new TermQuery(new Term("merchant_id", Integer.toString(merchant_id))), BooleanClause.Occur.SHOULD);
		}
		Filter filter = new CachingWrapperFilter(new QueryWrapperFilter(q2));
		
		String[] fields = {"name", "brand", "category", "description"};
		HashMap<String,Float> boosts = new HashMap<String,Float>();
		boosts.put("name", (float) 1.8);
		boosts.put("brand", (float) 1.2);
		boosts.put("category", (float) 1.1);
		boosts.put("description", (float) 0.5);
		
		queryString = queryString.replaceAll("[^\\p{L}\\p{N}]", " ");
		queryString = queryString.replaceAll("[ ]{2,}"," ").trim();
		if (queryString.length() == 0) {
			queryString = "*:*";//all
		}
		
		MultiFieldQueryParser qp = new MultiFieldQueryParser(Version.LUCENE_40, fields, server.getAnalizer(), boosts);
		qp.setDefaultOperator(Operator.OR);
		Query q1 = qp.parse(queryString);
		
		if (priceMin >= 0 && priceMax > 0) {
			BooleanQuery tmpq = new BooleanQuery();
			tmpq.add(q1, BooleanClause.Occur.MUST);
			tmpq.add(NumericRangeQuery.newDoubleRange("price", (double) priceMin, (double) priceMax, true, true), BooleanClause.Occur.MUST);
			q1 = tmpq.clone();
		}
		
		IndexSearcher searcher = server.getSearcher();
		TaxonomyReader taxo = server.getTaxoReader();
		TopScoreDocCollector topDocsCollector = TopScoreDocCollector.create(offset + limit, true);
		
		FacetSearchParams facetSearchParams = new FacetSearchParams();
		facetSearchParams.addFacetRequest(new CountFacetRequest(new CategoryPath("brand"), 10));
		facetSearchParams.addFacetRequest(new CountFacetRequest(new CategoryPath("category"), 10));
		facetSearchParams.addFacetRequest(new CountFacetRequest(new CategoryPath("merchant_id"), 10));
		
		FacetsCollector facetsCollector = new FacetsCollector(facetSearchParams, searcher.getIndexReader(), taxo);
		
		TotalHitCountCollector totalHitCollector = new TotalHitCountCollector();
		
		if (q2.getClauses().length > 0) {
			searcher.search(q1, filter, MultiCollector.wrap(topDocsCollector, facetsCollector, totalHitCollector));
		} else {
			searcher.search(q1, MultiCollector.wrap(topDocsCollector, facetsCollector, totalHitCollector));
		}
		
		TopDocs topDocs = topDocsCollector.topDocs();
		ScoreDoc[] hits = topDocs.scoreDocs;
		List<FacetResult> facetResults = facetsCollector.getFacetResults();
		
	//	DecimalFormat twoDForm = new DecimalFormat("0.000");
	//	String time = twoDForm.format((double) (System.currentTimeMillis() - timestamp) / 1000L);
		
		//System.out.println("Selected " + hits.length + " documents from " + totalHitCollector.getTotalHits() + " total hits " + " for query \"" + queryString + "\" in " + time + " sec.");
		
		JSONObject result = new JSONObject();
		String /*output, */key, value;
		JSONArray products = new JSONArray();
		JSONObject product;
		
		for(int i=offset; i < Math.min(offset + limit, hits.length); i++) {
		//	if (searcher.isDeleted(i))
		//		continue;
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			product = new JSONObject();
			
			List<IndexableField> l = d.getFields();
			if (l.size() > 0) {
			//	output = "";
				for (int j = 0; j < l.size(); j++) {
					key = l.get(j).name();
					value = d.get(key);
			//		output += ",##[" + key + "] -- [" + value + "]##";
					product.put(key, value);
				}
			//	output += " ##score - " + hits[i].score;
				product.put("score", hits[i].score);
			} //else {
			//	output = "no fields .. sad";
		//	}
			
		//	System.out.println((i + 1) + ". " + output);
			products.put(product);
		}
		result.put("products", products);
		
		JSONObject facetList = new JSONObject();
		JSONArray facets;
		JSONObject facet;
		for (FacetResult facetResult : facetResults) {
			FacetResultNode node = facetResult.getFacetResultNode();
		//	output = "";
			if (node.getNumSubResults() > 0) {
				facets = new JSONArray();
				for (FacetResultNode subnode : node.getSubResults()) {
					key = subnode. getLabel().lastComponent();
				//	output += key + " (" + subnode.getValue() + "), ";
					facet = new JSONObject();
					facet.put("name", key);
					facet.put("count", subnode.getValue());
					facets.put(facet);
				}
				facetList.put(node.getLabel().lastComponent(), facets);
			}// else {
			//	output = "no subnodes";
		//	}
		//	System.out.println("facet node: " + node.getLabel() + " - " + output);
		}
		result.put("facets", facetList);
		
		JSONObject stats = new JSONObject();
		stats.put("total_hits", totalHitCollector.getTotalHits());
		stats.put("total_documents", searcher.getIndexReader().numDocs());
		
		result.put("stats", stats);
		
		return result.toString();
	}
	
	@GET
	@Path("{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public String product(
			@PathParam("id") @DefaultValue("") String id,
			@Context HttpServletResponse response
		) throws IOException, JSONException {
		
		if(id.length() == 0 || id.length() > 100) {
			throw new WebJSONException(400, "Bad Request!");
		}
		
		LuceneServer server = LuceneServer.singleton();
		
		if(!server.isAlive()) {
			throw new WebJSONException(500, "Server is not alive!");
		}
		
		IndexSearcher searcher = server.getSearcher();
		JSONObject product = new JSONObject();
		String /*output = "", */key, value;
		
		int docId = -1;
		try {
			docId = Integer.parseInt(id);
		} catch (NumberFormatException e) {
			Term term;
			
			if (id.matches("([a-fA-F0-9]{32})")) {
				term = new Term("image_hash", id);
			} else {
				term = new Term("id", id);
			}
			TopDocs topDocs = searcher.search(new TermQuery(term), 1);
			ScoreDoc[] hits = topDocs.scoreDocs;
			if (hits.length == 0) {
				throw new WebJSONException(404, "Not Found");
			}
			docId = hits[0].doc;
		}
		
		Document d = searcher.doc(docId);
		product = new JSONObject();
		List<IndexableField> l = d.getFields();
		if (l.size() > 0) {
		//	output = "";
			for (int j = 0; j < l.size(); j++) {
				key = l.get(j).name();
				value = d.get(key);
			//	output += ",##[" + key + "] -- [" + value + "]##";
				product.put(key, value);
			}
		} else {
		//	output = "no fields .. sad";
		}
		
	//	System.out.println(output);
		
		return product.toString();
	}
}