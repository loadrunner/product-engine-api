package net.product_engine;

import javax.servlet.ServletException;

import net.product_engine.common.BaseServer;

import com.sun.jersey.spi.container.servlet.ServletContainer;

public class Servlet extends ServletContainer {
	private static final long serialVersionUID = 6323632100217630578L;
	
	@Override
	public void init() {
		try {
			super.init();
		} catch (ServletException e) {
			
		}
		
		BaseServer.init();
		LuceneServer.singleton();
		net.product_engine.importer.products.Server.singleton();
	}
}
